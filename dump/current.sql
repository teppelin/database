--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Debian 13.1-1.pgdg100+1)
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: teppelin_private; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA teppelin_private;


--
-- Name: teppelin_public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA teppelin_public;


--
-- Name: http_url; Type: DOMAIN; Schema: teppelin_public; Owner: -
--

CREATE DOMAIN teppelin_public.http_url AS text
	CONSTRAINT http_url_check CHECK ((VALUE ~ '^https?://[^/]+'::text));


--
-- Name: contact_info; Type: TYPE; Schema: teppelin_public; Owner: -
--

CREATE TYPE teppelin_public.contact_info AS (
	official_website teppelin_public.http_url,
	twitter teppelin_public.http_url,
	facebook teppelin_public.http_url,
	irc text,
	forum teppelin_public.http_url,
	discord text,
	mail text
);


--
-- Name: host_quality; Type: TYPE; Schema: teppelin_public; Owner: -
--

CREATE TYPE teppelin_public.host_quality AS ENUM (
    '240',
    '360',
    '480',
    '720',
    '1080',
    '1440',
    '2160'
);


--
-- Name: name_type; Type: TYPE; Schema: teppelin_public; Owner: -
--

CREATE TYPE teppelin_public.name_type AS ENUM (
    'main',
    'synonym',
    'official',
    'short',
    'native'
);


--
-- Name: season; Type: TYPE; Schema: teppelin_public; Owner: -
--

CREATE TYPE teppelin_public.season AS ENUM (
    'spring',
    'summer',
    'fall',
    'winter'
);


--
-- Name: watch_state; Type: TYPE; Schema: teppelin_public; Owner: -
--

CREATE TYPE teppelin_public.watch_state AS ENUM (
    'watching',
    'completed',
    'on_hold',
    'dropped',
    'plan_to_watch'
);


--
-- Name: assert_valid_password(text); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.assert_valid_password(new_password text) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
  -- TODO: add better assertions!
  if length(new_password) < 8 then
    raise exception 'Password is too weak' using errcode = 'WEAKP';
  end if;
end;
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: person; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.person (
    person_id integer NOT NULL,
    username public.citext NOT NULL,
    display_name text NOT NULL,
    avatar_url text,
    is_admin boolean DEFAULT false NOT NULL,
    is_verified boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT person_avatar_url_check CHECK ((avatar_url ~ '^https?://[^/]+'::text)),
    CONSTRAINT person_username_check CHECK (((length((username)::text) >= 2) AND (length((username)::text) <= 24) AND (username OPERATOR(public.~) '^[a-zA-Z]([a-zA-Z0-9][_]?)+$'::public.citext)))
);


--
-- Name: TABLE person; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.person IS '@omit all
A person who can log in to the application.';


--
-- Name: COLUMN person.person_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person.person_id IS 'Unique identifier for the person.';


--
-- Name: COLUMN person.username; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person.username IS 'Public-facing username (or ''handle'') of the person.';


--
-- Name: COLUMN person.display_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person.display_name IS 'Public-facing name (or pseudonym) of the person.';


--
-- Name: COLUMN person.avatar_url; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person.avatar_url IS 'Optional avatar URL.';


--
-- Name: COLUMN person.is_admin; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person.is_admin IS 'If true, the person has elevated privileges.';


--
-- Name: link_or_register_person(integer, character varying, character varying, json, json); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.link_or_register_person(f_person_id integer, f_service character varying, f_identifier character varying, f_profile json, f_auth_details json) RETURNS teppelin_public.person
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_matched_person_id int;
  v_matched_authentication_id int;
  v_email citext;
  v_display_name text;
  v_avatar_url text;
  v_person teppelin_public.person;
  v_person_email teppelin_public.person_email;
begin
  -- See if a person account already matches these details
  select person_authentication_id, person_id
    into v_matched_authentication_id, v_matched_person_id
    from teppelin_public.person_authentication
    where service = f_service
    and identifier = f_identifier
    limit 1;

  if v_matched_person_id is not null and f_person_id is not null and v_matched_person_id <> f_person_id then
    raise exception 'A different person already has this account linked.' using errcode='TAKEN';
  end if;

  v_email = f_profile ->> 'email';
  v_display_name := f_profile ->> 'display_name';
  v_avatar_url := f_profile ->> 'avatar_url';

  if v_matched_authentication_id is null then
    if f_person_id is not null then
      -- Link new account to logged in person account
      insert into teppelin_public.person_authentication (person_id, service, identifier, details) values
        (f_person_id, f_service, f_identifier, f_profile) returning id, person_id into v_matched_authentication_id, v_matched_person_id;
      insert into teppelin_private.person_authentication_secret (person_authentication_id, details) values
        (v_matched_authentication_id, f_auth_details);
        -- audit
        perform teppelin_private.send_message(
          'events',
          'person.linked_account',
          json_build_object(
            'type', 'linked_account',
            'person_id', f_person_id,
            'extra1', f_service,
            'extra2', f_identifier,
            'current_person_id', teppelin_public.current_person_id()
          )
        );
    elsif v_email is not null then
      -- See if the email is registered
      select * into v_person_email from teppelin_public.person_email where email = v_email and is_verified is true;
      if v_person_email is not null then
        -- person exists!
        insert into teppelin_public.person_authentication (person_id, service, identifier, details) values
          (v_person_email.person_id, f_service, f_identifier, f_profile) returning person_authentication_id, person_id into v_matched_authentication_id, v_matched_person_id;
        insert into teppelin_private.person_authentication_secret (person_authentication_id, details) values
          (v_matched_authentication_id, f_auth_details);
        perform teppelin_private.send_message(
          'events',
          'person.linked_account',
          json_build_object(
            'type', 'linked_account',
            'person_id', f_person_id,
            'extra1', f_service,
            'extra2', f_identifier,
            'current_person_id', teppelin_public.current_person_id()
          )
        );
      end if;
    end if;
  end if;
  if v_matched_person_id is null and f_person_id is null and v_matched_authentication_id is null then
    -- Create and return a new person account
    return teppelin_private.register_person(f_service, f_identifier, f_profile, f_auth_details, true);
  else
    if v_matched_authentication_id is not null then
      update teppelin_public.person_authentication
        set details = f_profile
        where person_authentication_id = v_matched_authentication_id;
      update teppelin_private.person_authentication_secret
        set details = f_auth_details
        where person_authentication_id = v_matched_authentication_id;
      update teppelin_public.person u
        set
          display_name = coalesce(u.display_name, v_display_name),
          avatar_url = coalesce(u.avatar_url, v_avatar_url)
        where person_id = v_matched_person_id
        returning * into v_person;
      return v_person;
    else
      -- v_matched_authentication_id is null
      -- -> v_matched_person_id is null (they're paired)
      -- -> f_person_id is not null (because the if clause above)
      -- -> v_matched_authentication_id is not null (because of the separate if block above creating a person_authentication)
      -- -> contradiction.
      raise exception 'This should not occur';
    end if;
  end if;
end;
$$;


--
-- Name: FUNCTION link_or_register_person(f_person_id integer, f_service character varying, f_identifier character varying, f_profile json, f_auth_details json); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.link_or_register_person(f_person_id integer, f_service character varying, f_identifier character varying, f_profile json, f_auth_details json) IS 'If you''re logged in, this will link an additional OAuth login to your account if necessary. If you''re logged out it may find if an account already exists (based on OAuth details or email address) and return that, or create a new person account if necessary.';


--
-- Name: session; Type: TABLE; Schema: teppelin_private; Owner: -
--

CREATE TABLE teppelin_private.session (
    uuid uuid DEFAULT gen_random_uuid() NOT NULL,
    person_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    last_active timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: login(text, text); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.login(username text, password text) RETURNS teppelin_private.session
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person teppelin_public.person;
  v_person_secret teppelin_private.person_secret;
  v_login_attempt_window_duration interval = interval '5 minutes';
  v_session teppelin_private.session;
begin
  if username like '%@%' then
    -- It's an email
    select person.* into v_person
    from teppelin_public.person
    inner join teppelin_public.person_email
    using (person_id)
    where person_email.email = login.username
    order by
      person_email.is_verified desc, -- Prefer verified email
      person_email.created_at asc -- Failing that, prefer the first registered (unverified person _should_ verify before logging in)
    limit 1;
  else
    -- It's a username
    select person.* into v_person
    from teppelin_public.person
    where person.username = login.username;
  end if;

  if not (v_person is null) then
    -- Load their secret
    select * into v_person_secret from teppelin_private.person_secret
    where person_secret.person_id = v_person.person_id;

    -- Have there been too many login attempts?
    if (
      v_person_secret.first_failed_password_attempt is not null
    and
      v_person_secret.first_failed_password_attempt > NOW() - v_login_attempt_window_duration
    and
      v_person_secret.failed_password_attempts >= 3
    ) then
      raise exception 'person account locked - too many login attempts. Try again after 5 minutes.' using errcode = 'LOCKD';
    end if;

    -- Not too many login attempts, let's check the password.
    -- NOTE: `password_hash` could be null, this is fine since `NULL = NULL` is null, and null is falsy.
    if v_person_secret.password_hash = crypt(password, v_person_secret.password_hash) then
      -- Excellent - they're logged in! Let's reset the attempt tracking
      update teppelin_private.person_secret
      set failed_password_attempts = 0, first_failed_password_attempt = null, last_login_at = now()
      where person_id = v_person.person_id;
      -- Create a session for the person
      insert into teppelin_private.session (person_id) values (v_person.person_id) returning * into v_session;
      -- And finally return the session
      return v_session;
    else
      -- Wrong password, bump all the attempt tracking figures
      update teppelin_private.person_secret
      set
        failed_password_attempts = (case when first_failed_password_attempt is null or first_failed_password_attempt < now() - v_login_attempt_window_duration then 1 else failed_password_attempts + 1 end),
        first_failed_password_attempt = (case when first_failed_password_attempt is null or first_failed_password_attempt < now() - v_login_attempt_window_duration then now() else first_failed_password_attempt end)
      where person_id = v_person.person_id;
      return null; -- Must not throw otherwise transaction will be aborted and attempts won't be recorded
    end if;
  else
    -- No person with that email/username was found
    return null;
  end if;
end;
$$;


--
-- Name: FUNCTION login(username text, password text); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.login(username text, password text) IS 'Returns a person that matches the username/password combo, or null on failure.';


--
-- Name: logout(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.logout() RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  -- Delete the session
  delete from teppelin_private.session where uuid = teppelin_public.current_session_id();
  -- Clear the identifier from the transaction
  perform set_config('jwt.claims.session_id', '', true);
end;
$$;


--
-- Name: really_create_person(public.citext, text, boolean, text, text, text); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.really_create_person(username public.citext, email text, email_is_verified boolean, display_name text, avatar_url text, password text DEFAULT NULL::text) RETURNS teppelin_public.person
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person teppelin_public.person;
  v_username citext = username;
begin
  if password is not null then
    perform teppelin_private.assert_valid_password(password);
  end if;
  if email is null then
    raise exception 'Email is required' using errcode = 'MODAT';
  end if;

  -- Insert the new person
  insert into teppelin_public.person (username, name, avatar_url) values
    (v_username, name, avatar_url)
    returning * into v_person;

	-- Add the person's email
  insert into teppelin_public.person_email (person_id, email, is_verified, is_primary)
  values (v_person.person_id, email, email_is_verified, email_is_verified);

  -- Store the password
  if password is not null then
    update teppelin_private.person_secrets
    set password_hash = crypt(password, gen_salt('bf'))
    where person_id = v_person.person_id;
  end if;

  -- Refresh the person
  select * into v_person from teppelin_public.person where person_id = v_person.person_id;

  return v_person;
end;
$$;


--
-- Name: FUNCTION really_create_person(username public.citext, email text, email_is_verified boolean, display_name text, avatar_url text, password text); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.really_create_person(username public.citext, email text, email_is_verified boolean, display_name text, avatar_url text, password text) IS 'Creates a person account. All arguments are optional, it trusts the calling method to perform sanitisation.';


--
-- Name: register_person(character varying, character varying, json, json, boolean); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.register_person(f_service character varying, f_identifier character varying, f_profile json, f_auth_details json, f_email_is_verified boolean DEFAULT false) RETURNS teppelin_public.person
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person teppelin_public.person;
  v_email citext;
  v_display_name text;
  v_username citext;
  v_avatar_url text;
  v_person_authentication_id uuid;
begin
  -- Extract data from the user’s OAuth profile data.
  v_email := f_profile ->> 'email';
  v_display_name := f_profile ->> 'display_name';
  v_username := f_profile ->> 'username';
  v_avatar_url := f_profile ->> 'avatar_url';

  -- Sanitise the username, and make it unique if necessary.
  if v_username is null then
    v_username = coalesce(v_name, 'person');
  end if;
  v_username = regexp_replace(v_username, '^[^a-z]+', '', 'i');
  v_username = regexp_replace(v_username, '[^a-z0-9]+', '_', 'i');
  if v_username is null or length(v_username) < 3 then
    v_username = 'person';
  end if;
  select (
    case
    when i = 0 then v_username
    else v_username || i::text
    end
  ) into v_username from generate_series(0, 1000) i
  where not exists(
    select 1
    from teppelin_public.person
    where person.username = (
      case
      when i = 0 then v_username
      else v_username || i::text
      end
    )
  )
  limit 1;

  -- Create the user account
  v_person = teppelin_private.really_create_user(
    username => v_username,
    email => v_email,
    email_is_verified => f_email_is_verified,
    display_name => v_name,
    avatar_url => v_avatar_url
  );

  -- Insert the user’s private account data (e.g. OAuth tokens)
  insert into teppelin_public.person_authentications (person_id, service, identifier, details) values
    (v_person.person_id, f_service, f_identifier, f_profile) returning person_id into v_person_authentication_id;
  insert into teppelin_private.person_authentication_secrets (person_authentication_id, details) values
    (v_person_authentication_id, f_auth_details);

  return v_person;
end;
$$;


--
-- Name: FUNCTION register_person(f_service character varying, f_identifier character varying, f_profile json, f_auth_details json, f_email_is_verified boolean); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.register_person(f_service character varying, f_identifier character varying, f_profile json, f_auth_details json, f_email_is_verified boolean) IS 'Used to register a user from information gleaned from OAuth. Primarily used by link_or_register_person';


--
-- Name: send_message(text, text, text); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.send_message(exchange text, routing_key text, message text) RETURNS void
    LANGUAGE sql STABLE
    AS $$
  -- TODO
  select 1;
	-- select amqp.publish(1, exchange, routing_key, message);
$$;


--
-- Name: tg__add_job_for_row(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.tg__add_job_for_row() RETURNS trigger
    LANGUAGE plpgsql STABLE
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
  declare
    routing_key text;
    row record;
  begin
    routing_key := 'row_change'
                   '.table-'::text || TG_TABLE_NAME::text || 
                   '.event-'::text || TG_OP::text;
    if (TG_OP = 'DELETE') then
        row := old;
    elsif (TG_OP = 'UPDATE') then
        row := new;
    elsif (TG_OP = 'INSERT') then
        row := new;
    end if;

    perform teppelin_private.send_message('events', routing_key, row_to_json(row)::text);
    return null;
  end;
$$;


--
-- Name: FUNCTION tg__add_job_for_row(); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.tg__add_job_for_row() IS 'Useful shortcut to create a job on insert or update. Pass the task name as the trigger argument, and the record id will automatically be available on the JSON payload.';


--
-- Name: tg__add_job_person_registered(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.tg__add_job_person_registered() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  perform teppelin_private.send_message('events', 'person.registered', json_build_object('person_id', NEW.person_id, 'email', NEW.email::text)::text);
  return NEW;
end;
$$;


--
-- Name: tg__set_season(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.tg__set_season() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  case
    when NEW.start_date between
        make_date(extract(year from NEW.start_date)::int, 3, 01)
        and
        make_date(extract(year from NEW.start_date)::int, 5, 31)
      then NEW.season := 'spring';
    when NEW.start_date between
        make_date(extract(year from NEW.start_date)::int, 6, 01)
        and
        make_date(extract(year from NEW.start_date)::int, 8, 31)
      then NEW.season := 'summer';
    when NEW.start_date between
        make_date(extract(year from NEW.start_date)::int, 9, 01)
        and
        make_date(extract(year from NEW.start_date)::int, 11, 30)
      then NEW.season := 'fall';
    else
      NEW.season := 'winter';
  end case;

  return NEW;
end;
$$;


--
-- Name: FUNCTION tg__set_season(); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.tg__set_season() IS 'A trigger that sets a season based on a date.';


--
-- Name: tg__timestamps(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.tg__timestamps() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  NEW.created_at = (case when TG_OP = 'INSERT' then NOW() else OLD.created_at end);
  NEW.updated_at = (case when TG_OP = 'UPDATE' and OLD.updated_at >= NOW() then OLD.updated_at + interval '1 millisecond' else NOW() end);
  return NEW;
end;
$$;


--
-- Name: FUNCTION tg__timestamps(); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.tg__timestamps() IS 'This trigger should be called on all tables with created_at, updated_at - it ensures that they cannot be manipulated and that updated_at will always be larger than the previous updated_at.';


--
-- Name: tg_person__make_first_person_admin(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.tg_person__make_first_person_admin() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  if not exists(select 1 from teppelin_public.person) then
    NEW.is_admin = true;
  end if;
  return NEW;
end;
$$;


--
-- Name: tg_person_email_secret__insert_with_person_email(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.tg_person_email_secret__insert_with_person_email() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_verification_token text;
begin
  if NEW.is_verified is false then
    v_verification_token = encode(gen_random_bytes(7), 'hex');
  end if;
  insert into teppelin_private.person_email_secret(person_email_id, verification_token) values(NEW.person_email_id, v_verification_token);
  return NEW;
end;
$$;


--
-- Name: FUNCTION tg_person_email_secret__insert_with_person_email(); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.tg_person_email_secret__insert_with_person_email() IS 'Ensures that every person_email record has an associated person_email_secret record.';


--
-- Name: tg_person_secret__insert_with_person(); Type: FUNCTION; Schema: teppelin_private; Owner: -
--

CREATE FUNCTION teppelin_private.tg_person_secret__insert_with_person() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  insert into teppelin_private.person_secret(person_id) values(NEW.person_id);
  return NEW;
end;
$$;


--
-- Name: FUNCTION tg_person_secret__insert_with_person(); Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON FUNCTION teppelin_private.tg_person_secret__insert_with_person() IS 'Ensures that every person record has an associated person_secret record.';


--
-- Name: add_message(text, text, json); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.add_message(exchange text, routing_key text, payload json) RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    AS $$
begin
  if teppelin_public.current_person_is_admin() is not true then
    raise exception 'Only admins have permission to do this.';
    return false;
  end if;

  perform teppelin_private.send_message(exchange, routing_key, payload::text);
  return true;
end;
$$;


--
-- Name: FUNCTION add_message(exchange text, routing_key text, payload json); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.add_message(exchange text, routing_key text, payload json) IS 'Sends a message into a channel, mostly for adding jobs (admin only).';


--
-- Name: change_password(text, text); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.change_password(old_password text, new_password text) RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person teppelin_public.person;
  v_person_secret teppelin_private.person_secret;
begin
  select person.* into v_person
  from teppelin_public.person
  where person_id = teppelin_public.current_person_id();

  if not (v_person is null) then
    -- Load their secret
    select * into v_person_secret from teppelin_private.person_secret
    where person_secret.person_id = v_person.person_id;

    if v_person_secret.password_hash = crypt(old_password, v_person_secret.password_hash) then
      perform teppelin_private.assert_valid_password(new_password);
      -- Reset the password as requested
      update teppelin_private.person_secret
      set
        password_hash = crypt(new_password, gen_salt('bf'))
      where person_secret.person_id = v_person.person_id;
        perform teppelin_private.send_message(
          'events',
          'person.change_password',
          json_build_object(
            'type', 'change_password',
            'person_id', v_person.person_id,
            'current_person_id', teppelin_public.current_person_id()
          )
        );
      return true;
    else
      raise exception 'Incorrect password' using errcode = 'CREDS';
    end if;
  else
    raise exception 'You must log in to change your password' using errcode = 'LOGIN';
  end if;
end;
$$;


--
-- Name: FUNCTION change_password(old_password text, new_password text); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.change_password(old_password text, new_password text) IS 'Enter your old password and a new password to change your password.';


--
-- Name: confirm_account_deletion(text); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.confirm_account_deletion(token text) RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person_secret teppelin_private.person_secret;
  v_token_max_duration interval = interval '3 days';
begin
  if teppelin_public.current_person_id() is null then
    raise exception 'You must log in to delete your account' using errcode = 'LOGIN';
  end if;

  select * into v_person_secret
    from teppelin_private.person_secret
    where person_secret.person_id = teppelin_public.current_person_id();

  if v_person_secret is null then
    -- Success: they're already deleted
    return true;
  end if;

  -- Check the token
  if (
    -- token is still valid
    v_person_secret.delete_account_token_generated > now() - v_token_max_duration
  and
    -- token matches
    v_person_secret.delete_account_token = token
  ) then
    -- Token passes; delete their account :(
    delete from teppelin_public.person where person_id = teppelin_public.current_person_id();
    return true;
  end if;

  raise exception 'The supplied token was incorrect - perhaps you''re logged in to the wrong account, or the token has expired?' using errcode = 'DNIED';
end;
$$;


--
-- Name: FUNCTION confirm_account_deletion(token text); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.confirm_account_deletion(token text) IS 'If you''re certain you want to delete your account, use `requestAccountDeletion` to request an account deletion token, and then supply the token through this mutation to complete account deletion.';


--
-- Name: current_person(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.current_person() RETURNS teppelin_public.person
    LANGUAGE sql STABLE
    AS $$
  select person.* from teppelin_public.person where person_id = teppelin_public.current_person_id();
$$;


--
-- Name: FUNCTION current_person(); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.current_person() IS 'The currently logged in person (or null if not logged in).';


--
-- Name: current_person_id(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.current_person_id() RETURNS integer
    LANGUAGE sql STABLE SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
  select person_id from teppelin_private.session where uuid = teppelin_public.current_session_id();
$$;


--
-- Name: FUNCTION current_person_id(); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.current_person_id() IS '@omit
Handy method to get the current person ID for use in RLS policies, etc; in GraphQL, use `currentperson{id}` instead.';


--
-- Name: current_person_is_admin(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.current_person_is_admin() RETURNS boolean
    LANGUAGE sql STABLE
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
  -- We're using exists here because it guarantees true/false rather than true/false/null
  select exists(
    select 1 from teppelin_public.person where person_id = teppelin_public.current_person_id() and is_admin = true
	);
$$;


--
-- Name: FUNCTION current_person_is_admin(); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.current_person_is_admin() IS '@omit
Handy method to determine if the current person is an admin, for use in RLS policies, etc; in GraphQL should use `currentperson{isAdmin}` instead.';


--
-- Name: current_session_id(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.current_session_id() RETURNS uuid
    LANGUAGE sql STABLE
    AS $$
  select nullif(pg_catalog.current_setting('jwt.claims.session_id', true), '')::uuid;
$$;


--
-- Name: FUNCTION current_session_id(); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.current_session_id() IS 'Handy method to get the current session ID.';


--
-- Name: forgot_password(public.citext); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.forgot_password(email public.citext) RETURNS void
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person_email teppelin_public.person_email;
  v_token text;
  v_token_min_duration_between_email interval = interval '3 minutes';
  v_token_max_duration interval = interval '3 days';
  v_now timestamptz = clock_timestamp(); -- Function can be called multiple during transaction
  v_latest_attempt timestamptz;
begin
  -- Find the matching person_email:
  select person_email.* into v_person_email
  from teppelin_public.person_email
  where person_email.email = forgot_password.email
  order by is_verified desc, id desc;

  -- If there is no match:
  if v_person_email is null then
    -- This email doesn't exist in the system; trigger an email stating as much.

    -- We do not allow this email to be triggered more than once every 15
    -- minutes, so we need to track it:
    insert into teppelin_private.unregistered_email_password_resets (email, latest_attempt)
      values (forgot_password.email, v_now)
      on conflict on constraint unregistered_email_pkey
      do update
        set latest_attempt = v_now, attempts = unregistered_email_password_resets.attempts + 1
        where unregistered_email_password_resets.latest_attempt < v_now - interval '15 minutes'
      returning latest_attempt into v_latest_attempt;

    if v_latest_attempt = v_now then
      perform teppelin_private.send_message(
        'events',
        'person.forgot_password_unregistered_email',
        json_build_object('email', forgot_password.email::text)
      );
    end if;

    -- TODO: we should clear out the unregistered_email_password_resets table periodically.

    return;
  end if;

  -- There was a match.
  -- See if we've triggered a reset recently:
  if exists(
    select 1
    from teppelin_private.person_email_secret
    where person_email_id = v_person_email.person_email_id
    and password_reset_email_sent_at is not null
    and password_reset_email_sent_at > v_now - v_token_min_duration_between_email
  ) then
    -- If so, take no action.
    return;
  end if;

  -- Fetch or generate reset token:
  update teppelin_private.person_secret
  set
    reset_password_token = (
      case
      when reset_password_token is null or reset_password_token_generated < v_now - v_token_max_duration
      then encode(gen_random_bytes(7), 'hex')
      else reset_password_token
      end
    ),
    reset_password_token_generated = (
      case
      when reset_password_token is null or reset_password_token_generated < v_now - v_token_max_duration
      then v_now
      else reset_password_token_generated
      end
    )
  where person_id = v_person_email.person_id
  returning reset_password_token into v_token;

  -- Don't allow spamming an email:
  update teppelin_private.person_email_secret
  set password_reset_email_sent_at = v_now
  where person_email_id = v_person_email.person_email_id;

  -- Trigger email send:
    perform teppelin_private.send_message(
      'events',
      'person.forgot_password',
      json_build_object('person_id', v_person_email.person_id, 'email', v_person_email.email::text, 'token', v_token)
    );


end;
$$;


--
-- Name: FUNCTION forgot_password(email public.citext); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.forgot_password(email public.citext) IS '@resultFieldName success
If you''ve forgotten your password, give us one of your email addresses and we''ll send you a reset token. Note this only works if you have added an email address!';


--
-- Name: person_email; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.person_email (
    person_email_id integer NOT NULL,
    person_id integer DEFAULT teppelin_public.current_person_id() NOT NULL,
    email public.citext NOT NULL,
    is_verified boolean DEFAULT false NOT NULL,
    is_primary boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT person_email_email_check CHECK ((email OPERATOR(public.~) '[^@]+@[^@]+\.[^@]+'::public.citext)),
    CONSTRAINT person_email_must_be_verified_to_be_primary CHECK (((is_primary IS FALSE) OR (is_verified IS TRUE)))
);


--
-- Name: TABLE person_email; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.person_email IS '@omit all
Information about a person''s email address.';


--
-- Name: COLUMN person_email.email; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_email.email IS 'The person email address, in `a@b.c` format.';


--
-- Name: COLUMN person_email.is_verified; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_email.is_verified IS 'True if the person has is_verified their email address (by clicking the link in the email we sent them, or logging in with a social login provider), false otherwise.';


--
-- Name: make_email_primary(uuid); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.make_email_primary(email_id uuid) RETURNS teppelin_public.person_email
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person_email teppelin_public.person_email;
begin
  select * into v_person_email from teppelin_public.person_email where id = email_id and person_id = teppelin_public.current_person_id();
  if v_person_email is null then
    raise exception 'That''s not your email' using errcode = 'DNIED';
    return null;
  end if;
  if v_person_email.is_verified is false then
    raise exception 'You may not make an unverified email primary' using errcode = 'VRFY1';
  end if;
  update teppelin_public.person_email set is_primary = false where person_id = teppelin_public.current_person_id() and is_primary is true and id <> email_id;
  update teppelin_public.person_email set is_primary = true where person_id = teppelin_public.current_person_id() and is_primary is not true and id = email_id returning * into v_person_email;
  return v_person_email;
end;
$$;


--
-- Name: FUNCTION make_email_primary(email_id uuid); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.make_email_primary(email_id uuid) IS 'Your primary email is where we''ll notify of account events; other emails may be used for discovery or login. Use this when you''re changing your email address.';


--
-- Name: person_has_password(teppelin_public.person); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.person_has_password(p teppelin_public.person) RETURNS boolean
    LANGUAGE sql STABLE SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
  select (password_hash is not null) from teppelin_private.person_secret where person_secret.person_id = p.person_id and p.person_id = teppelin_public.current_person_id();
$$;


--
-- Name: request_account_deletion(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.request_account_deletion() RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person_email teppelin_public.person_email;
  v_token text;
  v_token_max_duration interval = interval '3 days';
begin
  if teppelin_public.current_person_id() is null then
    raise exception 'You must log in to delete your account' using errcode = 'LOGIN';
  end if;

  -- Get the email to send account deletion token to
  select * into v_person_email
    from teppelin_public.person_email
    where person_id = teppelin_public.current_person_id()
    order by is_primary desc, is_verified desc, id desc
    limit 1;

  -- Fetch or generate token
  update teppelin_private.person_secret
  set
    delete_account_token = (
      case
      when delete_account_token is null or delete_account_token_generated < NOW() - v_token_max_duration
      then encode(gen_random_bytes(7), 'hex')
      else delete_account_token
      end
    ),
    delete_account_token_generated = (
      case
      when delete_account_token is null or delete_account_token_generated < NOW() - v_token_max_duration
      then now()
      else delete_account_token_generated
      end
    )
  where person_id = teppelin_public.current_person_id()
  returning delete_account_token into v_token;

  -- Trigger email send
  perform teppelin_private.send_message(
    'events',
    'person.send_delete_account_email',
    json_build_object('email', v_person_email.email::text, 'token', v_token)
  );
  return true;
end;
$$;


--
-- Name: FUNCTION request_account_deletion(); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.request_account_deletion() IS 'Begin the account deletion flow by requesting the confirmation email';


--
-- Name: resend_email_verification_code(uuid); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.resend_email_verification_code(email_id uuid) RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  if exists(
    select 1
    from teppelin_public.person_email
    where person_email.person_email_id = email_id
    and person_id = teppelin_public.current_person_id()
    and is_verified is false
  ) then
    perform teppelin_private.send_message(
      'events',
      'person_email.send_verification',
      json_build_object('person_email_id', email_id)
    );
    return true;
  end if;
  return false;
end;
$$;


--
-- Name: FUNCTION resend_email_verification_code(email_id uuid); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.resend_email_verification_code(email_id uuid) IS 'If you didn''t receive the verification code for this email, we can resend it. We silently cap the rate of resends on the backend, so calls to this function may not result in another email being sent if it has been called recently.';


--
-- Name: reset_password(integer, text, text); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.reset_password(person_id integer, reset_token text, new_password text) RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_person teppelin_public.person;
  v_person_secret teppelin_private.person_secret;
  v_reset_max_duration interval = interval '3 days';
begin
  select * into v_person
  from teppelin_public.person
  where person_id = person_id;

  if not (v_person is null) then
    -- Load their secret
    select * into v_person_secret from teppelin_private.person_secret
    where person_secret.person_id = v_person.person_id;

    -- Have there been too many reset attempts?
    if (
      v_person_secret.first_failed_reset_password_attempt is not null
    and
      v_person_secret.first_failed_reset_password_attempt > NOW() - v_reset_max_duration
    and
      v_person_secret.reset_password_attempts >= 20
    ) then
      raise exception 'Password reset locked - too many reset attempts' using errcode = 'LOCKD';
    end if;

    -- Not too many reset attempts, let's check the token
    if v_person_secret.reset_password_token = reset_token then
      -- Excellent - they're legit; 
      perform teppelin_private.assert_valid_password(new_password);
      -- let's reset the password as requested
      update teppelin_private.person_secret
      set
        password_hash = crypt(new_password, gen_salt('bf')),
        password_attempts = 0,
        first_failed_password_attempt = null,
        reset_password_token = null,
        reset_password_token_generated = null,
        reset_password_attempts = 0,
        first_failed_reset_password_attempt = null
      where person_secret.person_id = v_person.person_id;
      return true;
    else
      -- Wrong token, bump all the attempt tracking figures
      update teppelin_private.person_secret
      set
        reset_password_attempts = (case when first_failed_reset_password_attempt is null or first_failed_reset_password_attempt < now() - v_reset_max_duration then 1 else reset_password_attempts + 1 end),
        first_failed_reset_password_attempt = (case when first_failed_reset_password_attempt is null or first_failed_reset_password_attempt < now() - v_reset_max_duration then now() else first_failed_reset_password_attempt end)
      where person_secret.person_id = v_person.person_id;
      return null;
    end if;
  else
    -- No person with that id was found
    return null;
  end if;
end;
$$;


--
-- Name: FUNCTION reset_password(person_id integer, reset_token text, new_password text); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.reset_password(person_id integer, reset_token text, new_password text) IS 'After triggering forgotPassword, you''ll be sent a reset token. Combine this with your person ID and a new password to reset your password.';


--
-- Name: tg_chapter__update_media_chapter_count(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.tg_chapter__update_media_chapter_count() RETURNS trigger
    LANGUAGE plpgsql
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
declare
  v_media_id int;
begin
  v_media_id := v.media_id from teppelin_public.volume v where v.volume_id = NEW.volume_id;

  update teppelin_public.media m
  set chapter_count = cs.cnt
  from (
    select count(*) as cnt
    from teppelin_public.media m
    join teppelin_public.volume using (media_id)
    join teppelin_public.chapter using (volume_id)
    where m.media_id = v_media_id
  ) as cs
  where m.media_id = v_media_id;

  return null;
end;
$$;


--
-- Name: FUNCTION tg_chapter__update_media_chapter_count(); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.tg_chapter__update_media_chapter_count() IS 'Updates the chapter_count column on related media.';


--
-- Name: tg_person_email__forbid_if_verified(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.tg_person_email__forbid_if_verified() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  if exists(select 1 from teppelin_public.person_email where email = NEW.email and is_verified is true) then
    raise exception 'An account using that email address has already been created.' using errcode='EMTKN';
  end if;
  return NEW;
end;
$$;


--
-- Name: tg_person_email__prevent_delete_last_email(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.tg_person_email__prevent_delete_last_email() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  if exists (
    with remaining as (
      select person_email.person_id
      from teppelin_public.person_email
      inner join deleted
      using (person_id)
      -- Don't delete last verified email
      where (person_email.is_verified is true or not exists (
        select 1
        from deleted d2
        where d2.person_id = person_email.person_id
        and d2.is_verified is true
      ))
      order by person_email.person_email_id asc

      /*
       * Lock this table to prevent race conditions; see:
       * https://www.cybertec-postgresql.com/en/triggers-to-enforce-constraints/
       */
      for update of person_email
    )
    select 1
    from teppelin_public.person
    where person_id in (
      select person_id from deleted
      except
      select person_id from remaining
    )
  )
  then
    raise exception 'You must have at least one (verified) email address' using errcode = 'CDLEA';
  end if;

  return null;
end;
$$;


--
-- Name: tg_person_email__verify_account_on_verified(); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.tg_person_email__verify_account_on_verified() RETURNS trigger
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  update teppelin_public.users set is_verified = true where id = NEW.person_id and is_verified is false;
  return NEW;
end;
$$;


--
-- Name: verify_email(uuid, text); Type: FUNCTION; Schema: teppelin_public; Owner: -
--

CREATE FUNCTION teppelin_public.verify_email(f_person_email_id uuid, token text) RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public', 'pg_temp'
    AS $$
begin
  update teppelin_public.person_email
  set
    is_verified = true,
    is_primary = is_primary or not exists(
      select 1 from teppelin_public.person_email other_email where other_email.person_id = person_email.person_id and other_email.is_primary is true
    )
  where person_email_id = f_person_email_id
  and exists(
    select 1 from teppelin_private.person_email_secret where person_email_secret.person_email_id = person_email.person_email_id and verification_token = token
  );
  return found;
end;
$$;


--
-- Name: FUNCTION verify_email(f_person_email_id uuid, token text); Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON FUNCTION teppelin_public.verify_email(f_person_email_id uuid, token text) IS 'Once you have received a verification token for your email, you may call this mutation with that token to make your email verified.';


--
-- Name: connect_pg_simple_session; Type: TABLE; Schema: teppelin_private; Owner: -
--

CREATE TABLE teppelin_private.connect_pg_simple_session (
    sid character varying NOT NULL,
    sess json NOT NULL,
    expire timestamp without time zone NOT NULL
);


--
-- Name: person_authentication_secret; Type: TABLE; Schema: teppelin_private; Owner: -
--

CREATE TABLE teppelin_private.person_authentication_secret (
    person_authentication_id integer NOT NULL,
    details jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: person_email_secret; Type: TABLE; Schema: teppelin_private; Owner: -
--

CREATE TABLE teppelin_private.person_email_secret (
    person_email_id integer NOT NULL,
    verification_token text,
    verification_email_sent_at timestamp with time zone,
    password_reset_email_sent_at timestamp with time zone
);


--
-- Name: TABLE person_email_secret; Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON TABLE teppelin_private.person_email_secret IS 'The contents of this table should never be visible to the person. Contains data mostly related to email verification and avoiding spamming person.';


--
-- Name: COLUMN person_email_secret.password_reset_email_sent_at; Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON COLUMN teppelin_private.person_email_secret.password_reset_email_sent_at IS 'We store the time the last password reset was sent to this email to prevent the email getting flooded.';


--
-- Name: person_secret; Type: TABLE; Schema: teppelin_private; Owner: -
--

CREATE TABLE teppelin_private.person_secret (
    person_id integer NOT NULL,
    password_hash text,
    last_login_at timestamp with time zone DEFAULT now() NOT NULL,
    failed_password_attempts integer DEFAULT 0 NOT NULL,
    first_failed_password_attempt timestamp with time zone,
    reset_password_token text,
    reset_password_token_generated timestamp with time zone,
    failed_reset_password_attempts integer DEFAULT 0 NOT NULL,
    first_failed_reset_password_attempt timestamp with time zone,
    delete_account_token text,
    delete_account_token_generated timestamp with time zone
);


--
-- Name: TABLE person_secret; Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON TABLE teppelin_private.person_secret IS 'The contents of this table should never be visible to the person. Contains data mostly related to authentication.';


--
-- Name: unregistered_email_password_reset; Type: TABLE; Schema: teppelin_private; Owner: -
--

CREATE TABLE teppelin_private.unregistered_email_password_reset (
    email public.citext NOT NULL,
    attempts integer DEFAULT 1 NOT NULL,
    latest_attempt timestamp with time zone NOT NULL
);


--
-- Name: TABLE unregistered_email_password_reset; Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON TABLE teppelin_private.unregistered_email_password_reset IS 'If someone tries to recover the password for an email that is not registered in our system, this table enables us to rate-limit outgoing email to avoid spamming.';


--
-- Name: COLUMN unregistered_email_password_reset.attempts; Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON COLUMN teppelin_private.unregistered_email_password_reset.attempts IS 'We store the number of attempts to help us detect accounts being attacked.';


--
-- Name: COLUMN unregistered_email_password_reset.latest_attempt; Type: COMMENT; Schema: teppelin_private; Owner: -
--

COMMENT ON COLUMN teppelin_private.unregistered_email_password_reset.latest_attempt IS 'We store the time the last password reset was sent to this email to prevent the email getting flooded.';


--
-- Name: author; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.author (
    author_id integer NOT NULL,
    gender_id integer DEFAULT 0 NOT NULL,
    author_contact teppelin_public.contact_info,
    author_note text,
    author_photo teppelin_public.http_url,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE author; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.author IS 'A media author.';


--
-- Name: COLUMN author.gender_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author.gender_id IS 'The author''s gender.';


--
-- Name: COLUMN author.author_contact; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author.author_contact IS 'A composite type that describes author''s contact details.';


--
-- Name: COLUMN author.author_note; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author.author_note IS 'Some comment regarding this author.';


--
-- Name: COLUMN author.author_photo; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author.author_photo IS 'Author photo URL.';


--
-- Name: author_author_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.author ALTER COLUMN author_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.author_author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: author_mapping; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.author_mapping (
    author_id integer NOT NULL,
    cataloging_app_id integer NOT NULL,
    foreign_author_id text NOT NULL
);


--
-- Name: TABLE author_mapping; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.author_mapping IS '@omit all
Mapping from our author to a 3rd party author.';


--
-- Name: author_name; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.author_name (
    author_name_id integer NOT NULL,
    author_id integer NOT NULL,
    language_id integer,
    author_name text NOT NULL,
    author_name_type teppelin_public.name_type NOT NULL
);


--
-- Name: TABLE author_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.author_name IS 'An author''s name.';


--
-- Name: COLUMN author_name.author_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author_name.author_id IS 'Unique identifier for the author name.';


--
-- Name: COLUMN author_name.language_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author_name.language_id IS 'Language.';


--
-- Name: COLUMN author_name.author_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author_name.author_name IS 'The name.';


--
-- Name: COLUMN author_name.author_name_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.author_name.author_name_type IS 'The name type.';


--
-- Name: author_name_author_name_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.author_name ALTER COLUMN author_name_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.author_name_author_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cataloging_app; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.cataloging_app (
    cataloging_app_id integer NOT NULL,
    cataloging_app text NOT NULL,
    base_url text NOT NULL,
    resource_url text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE cataloging_app; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.cataloging_app IS 'An online anime database/cataloging application. Such as AniDB, MyAnimeList, NovelUpdates..';


--
-- Name: COLUMN cataloging_app.cataloging_app; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.cataloging_app.cataloging_app IS 'Name.';


--
-- Name: cataloging_app_cataloging_app_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.cataloging_app ALTER COLUMN cataloging_app_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.cataloging_app_cataloging_app_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: category; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.category (
    category_id smallint NOT NULL,
    category text NOT NULL
);


--
-- Name: TABLE category; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.category IS '@paginationCap -1
A main category on teppelin (novel, manga, anime).';


--
-- Name: COLUMN category.category; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.category.category IS 'The category name.';


--
-- Name: category_category_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.category ALTER COLUMN category_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: chapter; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.chapter (
    chapter_id integer NOT NULL,
    chapter_type_id smallint NOT NULL,
    volume_id integer NOT NULL,
    chapter_number integer NOT NULL,
    absolute_chapter_number text,
    chapter text NOT NULL,
    chapter_length integer,
    release_date date,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE chapter; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.chapter IS '@paginationCap 1000
A chapter belonging to a media.';


--
-- Name: COLUMN chapter.chapter_type_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter.chapter_type_id IS 'The type ID of the chapter.';


--
-- Name: COLUMN chapter.volume_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter.volume_id IS 'The local volume ID.';


--
-- Name: COLUMN chapter.chapter; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter.chapter IS 'A string identifier of this chapter.';


--
-- Name: COLUMN chapter.chapter_length; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter.chapter_length IS 'The length of the chapter - pages, minutes or null.';


--
-- Name: COLUMN chapter.release_date; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter.release_date IS 'The release date of the chapter.';


--
-- Name: chapter_chapter_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter ALTER COLUMN chapter_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.chapter_chapter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: chapter_mapping; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.chapter_mapping (
    chapter_id integer NOT NULL,
    cataloging_app_id integer NOT NULL,
    foreign_media_id text NOT NULL
);


--
-- Name: TABLE chapter_mapping; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.chapter_mapping IS '@omit all
Mapping from our chapter (episode) to a 3rd party media.';


--
-- Name: COLUMN chapter_mapping.chapter_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_mapping.chapter_id IS 'Chapter ref.';


--
-- Name: COLUMN chapter_mapping.cataloging_app_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_mapping.cataloging_app_id IS 'The 3rd party cataloging app this references to.';


--
-- Name: COLUMN chapter_mapping.foreign_media_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_mapping.foreign_media_id IS 'The 3rd party identifier.';


--
-- Name: chapter_thumbnail; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.chapter_thumbnail (
    chapter_thumbnail_id integer NOT NULL,
    chapter_id integer NOT NULL,
    thumbnail_url text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE chapter_thumbnail; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.chapter_thumbnail IS 'An media chapter_thumbnail.';


--
-- Name: COLUMN chapter_thumbnail.chapter_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_thumbnail.chapter_id IS 'the chapter this thumbnail belongs to.';


--
-- Name: chapter_thumbnail_chapter_thumbnail_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter_thumbnail ALTER COLUMN chapter_thumbnail_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.chapter_thumbnail_chapter_thumbnail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: chapter_title; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.chapter_title (
    chapter_title_id integer NOT NULL,
    chapter_id integer NOT NULL,
    language_id integer NOT NULL,
    chapter_title text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE chapter_title; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.chapter_title IS 'An media title.';


--
-- Name: COLUMN chapter_title.chapter_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_title.chapter_id IS 'The local chapter ID.';


--
-- Name: COLUMN chapter_title.language_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_title.language_id IS 'The language ID.';


--
-- Name: COLUMN chapter_title.chapter_title; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_title.chapter_title IS 'An media title.';


--
-- Name: chapter_title_chapter_title_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter_title ALTER COLUMN chapter_title_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.chapter_title_chapter_title_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: chapter_type; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.chapter_type (
    chapter_type_id integer NOT NULL,
    chapter_type text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE chapter_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.chapter_type IS 'Types an chapter can have.';


--
-- Name: COLUMN chapter_type.chapter_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.chapter_type.chapter_type IS 'The type name.';


--
-- Name: chapter_type_chapter_type_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter_type ALTER COLUMN chapter_type_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.chapter_type_chapter_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: credit_type; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.credit_type (
    credit_type_id integer NOT NULL,
    credit_type text NOT NULL,
    details json
);


--
-- Name: TABLE credit_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.credit_type IS 'A credit (creative arts) type.';


--
-- Name: credit_type_credit_type_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.credit_type ALTER COLUMN credit_type_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.credit_type_credit_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: gender; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.gender (
    gender_id smallint NOT NULL,
    gender text NOT NULL
);


--
-- Name: TABLE gender; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.gender IS 'A gender.';


--
-- Name: COLUMN gender.gender; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.gender.gender IS 'The gender...';


--
-- Name: genre; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.genre (
    genre_id integer NOT NULL,
    genre text NOT NULL,
    genre_description text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE genre; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.genre IS '@paginationCap -1
A Genre.';


--
-- Name: COLUMN genre.genre; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.genre.genre IS 'The genre name.';


--
-- Name: COLUMN genre.genre_description; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.genre.genre_description IS 'The genre description.';


--
-- Name: genre_genre_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.genre ALTER COLUMN genre_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.genre_genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: language; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.language (
    language_id integer NOT NULL,
    language_tag text NOT NULL,
    alpha2 text,
    alpha3 text,
    language text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE language; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.language IS '@paginationCap -1
Languages.';


--
-- Name: COLUMN language.language_tag; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.language.language_tag IS 'IETF/custom lanaguage tag.';


--
-- Name: COLUMN language.alpha2; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.language.alpha2 IS 'The language name.';


--
-- Name: language_language_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.language ALTER COLUMN language_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.language_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: media; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media (
    media_id integer NOT NULL,
    category_id smallint NOT NULL,
    media_state_id smallint NOT NULL,
    media_type_id smallint NOT NULL,
    language_id integer,
    status_in_coo text,
    synopsis text,
    start_date date,
    end_date date,
    season teppelin_public.season,
    chapter_count integer DEFAULT 0 NOT NULL,
    weighted_rating numeric DEFAULT 0 NOT NULL,
    average_rating numeric DEFAULT 0 NOT NULL,
    media_votes integer,
    licensed boolean DEFAULT false NOT NULL,
    completely_translated boolean DEFAULT false NOT NULL,
    nsfw boolean NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE media; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media IS '@paginationCap 50
A media.';


--
-- Name: COLUMN media.media_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.media_id IS 'The primary key for the media.';


--
-- Name: COLUMN media.category_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.category_id IS 'The media category (novel/manga/anime).';


--
-- Name: COLUMN media.media_state_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.media_state_id IS 'The status of the media (completed/ongoing/unknown).';


--
-- Name: COLUMN media.media_type_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.media_type_id IS 'The type of the media.';


--
-- Name: COLUMN media.status_in_coo; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.status_in_coo IS 'Status in country of origin';


--
-- Name: COLUMN media.synopsis; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.synopsis IS 'The description of the media (synopsis).';


--
-- Name: COLUMN media.start_date; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.start_date IS 'The start date of the media.';


--
-- Name: COLUMN media.end_date; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.end_date IS 'The end date of the media.';


--
-- Name: COLUMN media.season; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.season IS 'The season this media has aired in.';


--
-- Name: COLUMN media.chapter_count; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.chapter_count IS 'The number of chapters the media has.';


--
-- Name: COLUMN media.weighted_rating; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.weighted_rating IS 'The average rating of the media, based on cataloging apps.';


--
-- Name: COLUMN media.average_rating; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.average_rating IS 'The weighted score of the media, calculated from average.
https://myanimelist.net/info.php?go=topanime';


--
-- Name: COLUMN media.media_votes; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.media_votes IS 'Total number of votes.';


--
-- Name: COLUMN media.licensed; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.licensed IS 'Whether the media is licensed.';


--
-- Name: COLUMN media.completely_translated; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.completely_translated IS 'Whether the media is completely translated (has all chapters in a single language).';


--
-- Name: COLUMN media.nsfw; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media.nsfw IS 'Booean value, whether this media is NSFW.';


--
-- Name: media_author; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_author (
    media_id integer NOT NULL,
    author_id integer NOT NULL,
    credit_type_id integer NOT NULL
);


--
-- Name: TABLE media_author; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_author IS 'Junction table for media and authors';


--
-- Name: media_genre; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_genre (
    media_id integer NOT NULL,
    genre_id integer NOT NULL
);


--
-- Name: TABLE media_genre; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_genre IS 'Junction table for media and genres';


--
-- Name: media_mapping; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_mapping (
    media_id integer NOT NULL,
    cataloging_app_id integer NOT NULL,
    foreign_media_id text NOT NULL
);


--
-- Name: TABLE media_mapping; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_mapping IS '@omit all
Mapping from our media to a 3rd party media.';


--
-- Name: media_media_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media ALTER COLUMN media_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.media_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: media_publisher; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_publisher (
    media_id integer NOT NULL,
    publisher_id integer NOT NULL
);


--
-- Name: TABLE media_publisher; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_publisher IS 'Junction table for media and publishers';


--
-- Name: media_recommendation; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_recommendation (
    media1_id integer NOT NULL,
    media2_id integer NOT NULL,
    approvals integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE media_recommendation; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_recommendation IS 'Describes relations between media.
This is going to be modified in the future once we introduce our or recommendation system.';


--
-- Name: media_relation; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_relation (
    media1_id integer NOT NULL,
    media2_id integer NOT NULL,
    relation_id smallint NOT NULL
);


--
-- Name: TABLE media_relation; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_relation IS 'Describes relations between media.';


--
-- Name: media_state; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_state (
    media_state_id smallint NOT NULL,
    media_state text NOT NULL
);


--
-- Name: TABLE media_state; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_state IS 'The state of the media.';


--
-- Name: COLUMN media_state.media_state_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_state.media_state_id IS 'The primary unique identifier for the state.';


--
-- Name: COLUMN media_state.media_state; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_state.media_state IS 'The state name.';


--
-- Name: media_state_media_state_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_state ALTER COLUMN media_state_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.media_state_media_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: media_tag; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_tag (
    media_id integer NOT NULL,
    tag_id integer NOT NULL,
    tag_weight integer DEFAULT 200 NOT NULL
);


--
-- Name: TABLE media_tag; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_tag IS 'Junction table for media and tags';


--
-- Name: media_title; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_title (
    media_title_id integer NOT NULL,
    media_id integer NOT NULL,
    media_title text NOT NULL,
    language_id integer,
    media_name_type teppelin_public.name_type NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE media_title; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_title IS 'A media title.';


--
-- Name: COLUMN media_title.media_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_title.media_id IS 'Media ID.';


--
-- Name: COLUMN media_title.media_title; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_title.media_title IS 'The title.';


--
-- Name: COLUMN media_title.language_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_title.language_id IS 'Language ID.';


--
-- Name: COLUMN media_title.media_name_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_title.media_name_type IS 'The title type.';


--
-- Name: media_title_media_title_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_title ALTER COLUMN media_title_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.media_title_media_title_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: media_type; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.media_type (
    media_type_id smallint NOT NULL,
    media_type text NOT NULL
);


--
-- Name: TABLE media_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.media_type IS 'The type of the media.';


--
-- Name: COLUMN media_type.media_type_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_type.media_type_id IS 'The primary unique identifier for the type.';


--
-- Name: COLUMN media_type.media_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.media_type.media_type IS 'The type name.';


--
-- Name: media_type_media_type_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_type ALTER COLUMN media_type_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.media_type_media_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: person_authentication; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.person_authentication (
    person_authentication_id integer NOT NULL,
    person_id integer NOT NULL,
    service text NOT NULL,
    identifier text NOT NULL,
    details jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE person_authentication; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.person_authentication IS '@omit all
Contains information about the login providers this person has used, so that they may disconnect them should they wish.';


--
-- Name: COLUMN person_authentication.person_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_authentication.person_id IS '@omit';


--
-- Name: COLUMN person_authentication.service; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_authentication.service IS 'The login service used, e.g. `twitter` or `github`.';


--
-- Name: COLUMN person_authentication.identifier; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_authentication.identifier IS 'A unique identifier for the person within the login service.';


--
-- Name: COLUMN person_authentication.details; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_authentication.details IS '@omit
Additional profile details extracted from this login method';


--
-- Name: person_authentication_person_authentication_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person_authentication ALTER COLUMN person_authentication_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.person_authentication_person_authentication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: person_email_person_email_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person_email ALTER COLUMN person_email_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.person_email_person_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: person_person_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person ALTER COLUMN person_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.person_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: person_shelf; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.person_shelf (
    person_shelf_id integer NOT NULL,
    person_id integer DEFAULT teppelin_public.current_person_id() NOT NULL,
    category_id smallint NOT NULL,
    last_chapter_id integer,
    shelf_name text NOT NULL,
    public boolean DEFAULT false NOT NULL,
    cataloging_app_id integer,
    shelf_watch_state teppelin_public.watch_state,
    sync_shelf boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE person_shelf; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.person_shelf IS 'A media shelf belonging to a person.
 TODO: explain sync settings';


--
-- Name: COLUMN person_shelf.person_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_shelf.person_id IS 'The person this shelf belongs to.';


--
-- Name: COLUMN person_shelf.shelf_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_shelf.shelf_name IS 'Custom shelf name.';


--
-- Name: COLUMN person_shelf.public; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_shelf.public IS 'Whether other persons can view this shelf.';


--
-- Name: COLUMN person_shelf.cataloging_app_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_shelf.cataloging_app_id IS 'Ref to a cataloging app when sync is enabled.';


--
-- Name: COLUMN person_shelf.shelf_watch_state; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_shelf.shelf_watch_state IS 'Enum of watching states for anime type shelves.';


--
-- Name: COLUMN person_shelf.sync_shelf; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.person_shelf.sync_shelf IS 'Set to true to enable synchronization with cataloging_app.';


--
-- Name: person_shelf_person_shelf_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person_shelf ALTER COLUMN person_shelf_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.person_shelf_person_shelf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: poster; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.poster (
    poster_id integer NOT NULL,
    media_id integer NOT NULL,
    main boolean DEFAULT false NOT NULL,
    poster_url text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE poster; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.poster IS 'An media poster.';


--
-- Name: COLUMN poster.media_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.poster.media_id IS 'The local media ID.';


--
-- Name: poster_poster_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.poster ALTER COLUMN poster_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.poster_poster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: publisher; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.publisher (
    publisher_id integer NOT NULL,
    language_id integer,
    publisher_contact teppelin_public.contact_info,
    publisher_note text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE publisher; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.publisher IS 'A media publisher.';


--
-- Name: COLUMN publisher.publisher_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher.publisher_id IS 'Unique identifier for the publisher.';


--
-- Name: COLUMN publisher.language_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher.language_id IS 'The main language the publications are in.';


--
-- Name: COLUMN publisher.publisher_contact; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher.publisher_contact IS 'Publisher contact details.';


--
-- Name: COLUMN publisher.publisher_note; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher.publisher_note IS 'A text note.';


--
-- Name: publisher_mapping; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.publisher_mapping (
    publisher_id integer NOT NULL,
    cataloging_app_id integer NOT NULL,
    foreign_publisher_id text NOT NULL
);


--
-- Name: TABLE publisher_mapping; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.publisher_mapping IS '@omit all
Mapping from our publisher to a 3rd party publisher.';


--
-- Name: publisher_name; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.publisher_name (
    publisher_name_id integer NOT NULL,
    publisher_id integer NOT NULL,
    language_id integer,
    publisher_name text NOT NULL,
    publisher_name_type teppelin_public.name_type NOT NULL
);


--
-- Name: TABLE publisher_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.publisher_name IS 'A publisher name.';


--
-- Name: COLUMN publisher_name.publisher_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher_name.publisher_id IS 'The publisher this name belongs to.';


--
-- Name: COLUMN publisher_name.language_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher_name.language_id IS 'Language.';


--
-- Name: COLUMN publisher_name.publisher_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher_name.publisher_name IS 'The name.';


--
-- Name: COLUMN publisher_name.publisher_name_type; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.publisher_name.publisher_name_type IS 'The name type.';


--
-- Name: publisher_name_publisher_name_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.publisher_name ALTER COLUMN publisher_name_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.publisher_name_publisher_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: publisher_publisher_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.publisher ALTER COLUMN publisher_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.publisher_publisher_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: relation; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.relation (
    relation_id smallint NOT NULL,
    relation text NOT NULL
);


--
-- Name: TABLE relation; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.relation IS '@paginationCap -1
sequel, prequel, adaption, same franchise, same universe..';


--
-- Name: COLUMN relation.relation; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.relation.relation IS 'the relation name.';


--
-- Name: relation_relation_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.relation ALTER COLUMN relation_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.relation_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: release; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.release (
    release_id integer NOT NULL,
    chapter_id integer NOT NULL,
    release_group_id integer NOT NULL,
    language_id integer NOT NULL,
    content text,
    source_url teppelin_public.http_url,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    translator text
);


--
-- Name: TABLE release; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.release IS '@paginationCap 20
A media chapter release. Can be either an anime episode, manga chapter or a novel chapter.';


--
-- Name: COLUMN release.chapter_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.release.chapter_id IS 'The chapter the release belongs to.';


--
-- Name: release_group; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.release_group (
    release_group_id integer NOT NULL,
    release_group_name text NOT NULL,
    release_group_contact teppelin_public.contact_info,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE release_group; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.release_group IS 'An release_group.';


--
-- Name: COLUMN release_group.release_group_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.release_group.release_group_name IS 'The main release_group name.';


--
-- Name: COLUMN release_group.release_group_contact; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.release_group.release_group_contact IS 'release_group contact details.';


--
-- Name: release_group_mapping; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.release_group_mapping (
    release_group_id integer NOT NULL,
    cataloging_app_id integer NOT NULL,
    foreign_release_group_id text NOT NULL
);


--
-- Name: TABLE release_group_mapping; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.release_group_mapping IS '@omit all
Mapping from our release group to a 3rd party release group.';


--
-- Name: release_group_release_group_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.release_group ALTER COLUMN release_group_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.release_group_release_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: release_mapping; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.release_mapping (
    release_id integer NOT NULL,
    cataloging_app_id integer NOT NULL,
    foreign_release_id text NOT NULL
);


--
-- Name: TABLE release_mapping; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.release_mapping IS '@omit all
Mapping from our release to a 3rd party release.';


--
-- Name: release_release_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.release ALTER COLUMN release_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.release_release_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tag; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.tag (
    tag_id integer NOT NULL,
    tag text NOT NULL,
    tag_description text NOT NULL
);


--
-- Name: TABLE tag; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.tag IS '@paginationCap -1
A tag.';


--
-- Name: COLUMN tag.tag; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.tag.tag IS 'The tag name.';


--
-- Name: COLUMN tag.tag_description; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.tag.tag_description IS 'The tag description.';


--
-- Name: tag_tag_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.tag ALTER COLUMN tag_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.tag_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: volume; Type: TABLE; Schema: teppelin_public; Owner: -
--

CREATE TABLE teppelin_public.volume (
    volume_id integer NOT NULL,
    media_id integer NOT NULL,
    volume_number integer NOT NULL,
    volume_name text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE volume; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON TABLE teppelin_public.volume IS 'A volume belonging to an media.';


--
-- Name: COLUMN volume.media_id; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.volume.media_id IS 'The local media ID.';


--
-- Name: COLUMN volume.volume_number; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.volume.volume_number IS 'Unsigned int volume number.';


--
-- Name: COLUMN volume.volume_name; Type: COMMENT; Schema: teppelin_public; Owner: -
--

COMMENT ON COLUMN teppelin_public.volume.volume_name IS 'The volume name.';


--
-- Name: volume_volume_id_seq; Type: SEQUENCE; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.volume ALTER COLUMN volume_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME teppelin_public.volume_volume_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: person_authentication_secret person_authentication_secret_pkey; Type: CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.person_authentication_secret
    ADD CONSTRAINT person_authentication_secret_pkey PRIMARY KEY (person_authentication_id);


--
-- Name: person_email_secret person_email_secret_pkey; Type: CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.person_email_secret
    ADD CONSTRAINT person_email_secret_pkey PRIMARY KEY (person_email_id);


--
-- Name: person_secret person_secret_pkey; Type: CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.person_secret
    ADD CONSTRAINT person_secret_pkey PRIMARY KEY (person_id);


--
-- Name: connect_pg_simple_session session_pkey; Type: CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.connect_pg_simple_session
    ADD CONSTRAINT session_pkey PRIMARY KEY (sid);


--
-- Name: session session_pkey1; Type: CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.session
    ADD CONSTRAINT session_pkey1 PRIMARY KEY (uuid);


--
-- Name: unregistered_email_password_reset unregistered_email_pkey; Type: CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.unregistered_email_password_reset
    ADD CONSTRAINT unregistered_email_pkey PRIMARY KEY (email);


--
-- Name: author_mapping author_mapping_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author_mapping
    ADD CONSTRAINT author_mapping_pkey PRIMARY KEY (author_id, cataloging_app_id);


--
-- Name: author_name author_name_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author_name
    ADD CONSTRAINT author_name_pkey PRIMARY KEY (author_name_id);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (author_id);


--
-- Name: cataloging_app cataloging_app_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.cataloging_app
    ADD CONSTRAINT cataloging_app_pkey PRIMARY KEY (cataloging_app_id);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category_id);


--
-- Name: chapter_mapping chapter_mapping_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_mapping
    ADD CONSTRAINT chapter_mapping_pkey PRIMARY KEY (chapter_id, cataloging_app_id);


--
-- Name: chapter chapter_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter
    ADD CONSTRAINT chapter_pkey PRIMARY KEY (chapter_id);


--
-- Name: chapter_thumbnail chapter_thumbnail_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_thumbnail
    ADD CONSTRAINT chapter_thumbnail_pkey PRIMARY KEY (chapter_thumbnail_id);


--
-- Name: chapter_title chapter_title_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_title
    ADD CONSTRAINT chapter_title_pkey PRIMARY KEY (chapter_title_id);


--
-- Name: chapter_type chapter_type_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_type
    ADD CONSTRAINT chapter_type_pkey PRIMARY KEY (chapter_type_id);


--
-- Name: chapter chapter_volume_id_chapter_number_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter
    ADD CONSTRAINT chapter_volume_id_chapter_number_key UNIQUE (volume_id, chapter_number);


--
-- Name: credit_type credit_type_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.credit_type
    ADD CONSTRAINT credit_type_pkey PRIMARY KEY (credit_type_id);


--
-- Name: gender gender_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.gender
    ADD CONSTRAINT gender_pkey PRIMARY KEY (gender_id);


--
-- Name: genre genre_genre_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.genre
    ADD CONSTRAINT genre_genre_key UNIQUE (genre);


--
-- Name: genre genre_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.genre
    ADD CONSTRAINT genre_pkey PRIMARY KEY (genre_id);


--
-- Name: language language_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.language
    ADD CONSTRAINT language_pkey PRIMARY KEY (language_id);


--
-- Name: media_author media_author_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_author
    ADD CONSTRAINT media_author_pkey PRIMARY KEY (media_id, author_id);


--
-- Name: media_genre media_genre_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_genre
    ADD CONSTRAINT media_genre_pkey PRIMARY KEY (media_id, genre_id);


--
-- Name: media_mapping media_mapping_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_mapping
    ADD CONSTRAINT media_mapping_pkey PRIMARY KEY (media_id, cataloging_app_id);


--
-- Name: media media_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (media_id);


--
-- Name: media_publisher media_publisher_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_publisher
    ADD CONSTRAINT media_publisher_pkey PRIMARY KEY (media_id, publisher_id);


--
-- Name: media_recommendation media_recommendation_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_recommendation
    ADD CONSTRAINT media_recommendation_pkey PRIMARY KEY (media1_id, media2_id);


--
-- Name: media_relation media_relation_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_relation
    ADD CONSTRAINT media_relation_pkey PRIMARY KEY (media1_id, media2_id, relation_id);


--
-- Name: media_state media_state_media_state_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_state
    ADD CONSTRAINT media_state_media_state_key UNIQUE (media_state);


--
-- Name: media_state media_state_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_state
    ADD CONSTRAINT media_state_pkey PRIMARY KEY (media_state_id);


--
-- Name: media_tag media_tag_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_tag
    ADD CONSTRAINT media_tag_pkey PRIMARY KEY (media_id, tag_id);


--
-- Name: media_title media_title_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_title
    ADD CONSTRAINT media_title_pkey PRIMARY KEY (media_title_id);


--
-- Name: media_type media_type_media_type_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_type
    ADD CONSTRAINT media_type_media_type_key UNIQUE (media_type);


--
-- Name: media_type media_type_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_type
    ADD CONSTRAINT media_type_pkey PRIMARY KEY (media_type_id);


--
-- Name: person_authentication person_authentication_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_authentication
    ADD CONSTRAINT person_authentication_pkey PRIMARY KEY (person_authentication_id);


--
-- Name: person_email person_email_person_id_email_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_email
    ADD CONSTRAINT person_email_person_id_email_key UNIQUE (person_id, email);


--
-- Name: person_email person_email_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_email
    ADD CONSTRAINT person_email_pkey PRIMARY KEY (person_email_id);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (person_id);


--
-- Name: person_shelf person_shelf_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_shelf
    ADD CONSTRAINT person_shelf_pkey PRIMARY KEY (person_shelf_id);


--
-- Name: person person_username_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person
    ADD CONSTRAINT person_username_key UNIQUE (username);


--
-- Name: poster poster_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.poster
    ADD CONSTRAINT poster_pkey PRIMARY KEY (poster_id);


--
-- Name: publisher_mapping publisher_mapping_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher_mapping
    ADD CONSTRAINT publisher_mapping_pkey PRIMARY KEY (publisher_id, cataloging_app_id);


--
-- Name: publisher_name publisher_name_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher_name
    ADD CONSTRAINT publisher_name_pkey PRIMARY KEY (publisher_name_id);


--
-- Name: publisher publisher_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher
    ADD CONSTRAINT publisher_pkey PRIMARY KEY (publisher_id);


--
-- Name: relation relation_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.relation
    ADD CONSTRAINT relation_pkey PRIMARY KEY (relation_id);


--
-- Name: release release_chapter_id_release_group_id_language_id_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release
    ADD CONSTRAINT release_chapter_id_release_group_id_language_id_key UNIQUE (chapter_id, release_group_id, language_id);


--
-- Name: release_group_mapping release_group_mapping_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release_group_mapping
    ADD CONSTRAINT release_group_mapping_pkey PRIMARY KEY (release_group_id, cataloging_app_id, foreign_release_group_id);


--
-- Name: release_group release_group_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release_group
    ADD CONSTRAINT release_group_pkey PRIMARY KEY (release_group_id);


--
-- Name: release_mapping release_mapping_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release_mapping
    ADD CONSTRAINT release_mapping_pkey PRIMARY KEY (release_id, cataloging_app_id);


--
-- Name: release release_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release
    ADD CONSTRAINT release_pkey PRIMARY KEY (release_id);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (tag_id);


--
-- Name: tag tag_tag_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.tag
    ADD CONSTRAINT tag_tag_key UNIQUE (tag);


--
-- Name: person_authentication uniq_person_authentication; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_authentication
    ADD CONSTRAINT uniq_person_authentication UNIQUE (service, identifier);


--
-- Name: volume volume_media_id_volume_number_key; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.volume
    ADD CONSTRAINT volume_media_id_volume_number_key UNIQUE (media_id, volume_number);


--
-- Name: volume volume_pkey; Type: CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.volume
    ADD CONSTRAINT volume_pkey PRIMARY KEY (volume_id);


--
-- Name: session_person_id_idx; Type: INDEX; Schema: teppelin_private; Owner: -
--

CREATE INDEX session_person_id_idx ON teppelin_private.session USING btree (person_id);


--
-- Name: author_gender_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX author_gender_id_idx ON teppelin_public.author USING btree (gender_id);


--
-- Name: author_mapping_cataloging_app_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX author_mapping_cataloging_app_id_idx ON teppelin_public.author_mapping USING btree (cataloging_app_id);


--
-- Name: author_name_author_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX author_name_author_id_idx ON teppelin_public.author_name USING btree (author_id);


--
-- Name: author_name_language_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX author_name_language_id_idx ON teppelin_public.author_name USING btree (language_id);


--
-- Name: chapter_chapter_type_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX chapter_chapter_type_id_idx ON teppelin_public.chapter USING btree (chapter_type_id);


--
-- Name: chapter_mapping_cataloging_app_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX chapter_mapping_cataloging_app_id_idx ON teppelin_public.chapter_mapping USING btree (cataloging_app_id);


--
-- Name: chapter_thumbnail_chapter_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX chapter_thumbnail_chapter_id_idx ON teppelin_public.chapter_thumbnail USING btree (chapter_id);


--
-- Name: chapter_title_chapter_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX chapter_title_chapter_id_idx ON teppelin_public.chapter_title USING btree (chapter_id);


--
-- Name: chapter_title_language_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX chapter_title_language_id_idx ON teppelin_public.chapter_title USING btree (language_id);

--
-- Name: idx_person_email_primary; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX idx_person_email_primary ON teppelin_public.person_email USING btree (is_primary, person_id);


--
-- Name: media_author_author_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_author_author_id_idx ON teppelin_public.media_author USING btree (author_id);


--
-- Name: media_average_rating_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_average_rating_idx ON teppelin_public.media USING btree (average_rating);


--
-- Name: media_category_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_category_id_idx ON teppelin_public.media USING btree (category_id);


--
-- Name: media_chapter_count_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_chapter_count_idx ON teppelin_public.media USING btree (chapter_count);


--
-- Name: media_genre_genre_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_genre_genre_id_idx ON teppelin_public.media_genre USING btree (genre_id);


--
-- Name: media_language_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_language_id_idx ON teppelin_public.media USING btree (language_id);


--
-- Name: media_mapping_cataloging_app_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_mapping_cataloging_app_id_idx ON teppelin_public.media_mapping USING btree (cataloging_app_id);


--
-- Name: media_media_state_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_media_state_id_idx ON teppelin_public.media USING btree (media_state_id);


--
-- Name: media_media_type_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_media_type_id_idx ON teppelin_public.media USING btree (media_type_id);


--
-- Name: media_publisher_publisher_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_publisher_publisher_id_idx ON teppelin_public.media_publisher USING btree (publisher_id);


--
-- Name: media_recommendation_media2_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_recommendation_media2_id_idx ON teppelin_public.media_recommendation USING btree (media2_id);


--
-- Name: media_relation_media2_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_relation_media2_id_idx ON teppelin_public.media_relation USING btree (media2_id);


--
-- Name: media_relation_relation_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_relation_relation_id_idx ON teppelin_public.media_relation USING btree (relation_id);


--
-- Name: media_tag_tag_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_tag_tag_id_idx ON teppelin_public.media_tag USING btree (tag_id);


--
-- Name: media_title_language_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_title_language_id_idx ON teppelin_public.media_title USING btree (language_id);


--
-- Name: media_title_media_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_title_media_id_idx ON teppelin_public.media_title USING btree (media_id);


--
-- Name: media_title_media_name_type_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_title_media_name_type_idx ON teppelin_public.media_title USING btree (media_name_type);


--
-- Name: media_weighted_rating_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX media_weighted_rating_idx ON teppelin_public.media USING btree (weighted_rating);


--
-- Name: person_authentication_person_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX person_authentication_person_id_idx ON teppelin_public.person_authentication USING btree (person_id);


--
-- Name: person_shelf_cataloging_app_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX person_shelf_cataloging_app_id_idx ON teppelin_public.person_shelf USING btree (cataloging_app_id);


--
-- Name: person_shelf_category_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX person_shelf_category_id_idx ON teppelin_public.person_shelf USING btree (category_id);


--
-- Name: person_shelf_last_chapter_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX person_shelf_last_chapter_id_idx ON teppelin_public.person_shelf USING btree (last_chapter_id);


--
-- Name: person_shelf_person_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX person_shelf_person_id_idx ON teppelin_public.person_shelf USING btree (person_id);


--
-- Name: poster_media_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX poster_media_id_idx ON teppelin_public.poster USING btree (media_id);


--
-- Name: publisher_language_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX publisher_language_id_idx ON teppelin_public.publisher USING btree (language_id);


--
-- Name: publisher_mapping_cataloging_app_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX publisher_mapping_cataloging_app_id_idx ON teppelin_public.publisher_mapping USING btree (cataloging_app_id);


--
-- Name: publisher_name_language_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX publisher_name_language_id_idx ON teppelin_public.publisher_name USING btree (language_id);


--
-- Name: publisher_name_publisher_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX publisher_name_publisher_id_idx ON teppelin_public.publisher_name USING btree (publisher_id);


--
-- Name: release_group_mapping_cataloging_app_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX release_group_mapping_cataloging_app_id_idx ON teppelin_public.release_group_mapping USING btree (cataloging_app_id);


--
-- Name: release_language_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX release_language_id_idx ON teppelin_public.release USING btree (language_id);


--
-- Name: release_mapping_cataloging_app_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX release_mapping_cataloging_app_id_idx ON teppelin_public.release_mapping USING btree (cataloging_app_id);


--
-- Name: release_release_group_id_idx; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX release_release_group_id_idx ON teppelin_public.release USING btree (release_group_id);


--
-- Name: trgm_idx_media_title_title; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE INDEX trgm_idx_media_title_title ON teppelin_public.media_title USING gin (media_title public.gin_trgm_ops);


--
-- Name: uniq_author_name_main; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE UNIQUE INDEX uniq_author_name_main ON teppelin_public.author_name USING btree (author_id, author_name_type) WHERE (author_name_type = 'main'::teppelin_public.name_type);


--
-- Name: uniq_person_email_primary_email; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE UNIQUE INDEX uniq_person_email_primary_email ON teppelin_public.person_email USING btree (person_id) WHERE (is_primary IS TRUE);


--
-- Name: uniq_person_email_verified_email; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE UNIQUE INDEX uniq_person_email_verified_email ON teppelin_public.person_email USING btree (email) WHERE (is_verified IS TRUE);


--
-- Name: uniq_publisher_name_main; Type: INDEX; Schema: teppelin_public; Owner: -
--

CREATE UNIQUE INDEX uniq_publisher_name_main ON teppelin_public.publisher_name USING btree (publisher_id, publisher_name_type) WHERE (publisher_name_type = 'main'::teppelin_public.name_type);


--
-- Name: media _050_season; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _050_season BEFORE INSERT OR UPDATE ON teppelin_public.media FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__set_season();


--
-- Name: author _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.author FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: cataloging_app _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.cataloging_app FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: chapter _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.chapter FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: chapter_thumbnail _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.chapter_thumbnail FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: chapter_title _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.chapter_title FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: chapter_type _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.chapter_type FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: genre _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.genre FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: language _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.language FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: media _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.media FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: media_title _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.media_title FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: person _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps AFTER INSERT OR UPDATE ON teppelin_public.person FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: person_authentication _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps AFTER INSERT OR UPDATE ON teppelin_public.person_authentication FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: person_email _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps AFTER INSERT OR UPDATE ON teppelin_public.person_email FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: person_shelf _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.person_shelf FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: poster _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.poster FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: publisher _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.publisher FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: release _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.release FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: release_group _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.release_group FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: volume _100_timestamps; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _100_timestamps BEFORE INSERT OR UPDATE ON teppelin_public.volume FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg__timestamps();


--
-- Name: person_email _200_forbid_existing_email; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _200_forbid_existing_email BEFORE INSERT ON teppelin_public.person_email FOR EACH ROW EXECUTE FUNCTION teppelin_public.tg_person_email__forbid_if_verified();


--
-- Name: person _200_make_first_person_admin; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _200_make_first_person_admin BEFORE INSERT ON teppelin_public.person FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg_person__make_first_person_admin();


--
-- Name: chapter _200_update_media_chapter_count; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _200_update_media_chapter_count AFTER INSERT ON teppelin_public.chapter FOR EACH ROW EXECUTE FUNCTION teppelin_public.tg_chapter__update_media_chapter_count();


--
-- Name: person _500_insert_secret; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _500_insert_secret AFTER INSERT ON teppelin_public.person FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg_person_secret__insert_with_person();


--
-- Name: person_email _500_insert_secret; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _500_insert_secret AFTER INSERT ON teppelin_public.person_email FOR EACH ROW EXECUTE FUNCTION teppelin_private.tg_person_email_secret__insert_with_person_email();


--
-- Name: person_email _500_prevent_delete_last; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _500_prevent_delete_last AFTER DELETE ON teppelin_public.person_email REFERENCING OLD TABLE AS deleted FOR EACH STATEMENT EXECUTE FUNCTION teppelin_public.tg_person_email__prevent_delete_last_email();


--
-- Name: person_email _500_verify_account_on_verified; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _500_verify_account_on_verified AFTER INSERT OR UPDATE OF is_verified ON teppelin_public.person_email FOR EACH ROW WHEN ((new.is_verified IS TRUE)) EXECUTE FUNCTION teppelin_public.tg_person_email__verify_account_on_verified();


--
-- Name: person_email _900_send_verification_email; Type: TRIGGER; Schema: teppelin_public; Owner: -
--

CREATE TRIGGER _900_send_verification_email AFTER INSERT ON teppelin_public.person_email FOR EACH ROW WHEN ((new.is_verified IS FALSE)) EXECUTE FUNCTION teppelin_private.tg__add_job_person_registered();


--
-- Name: person_authentication_secret person_authentication_secret_person_authentication_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.person_authentication_secret
    ADD CONSTRAINT person_authentication_secret_person_authentication_id_fkey FOREIGN KEY (person_authentication_id) REFERENCES teppelin_public.person_authentication(person_authentication_id) ON DELETE CASCADE;


--
-- Name: person_email_secret person_email_secret_person_email_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.person_email_secret
    ADD CONSTRAINT person_email_secret_person_email_id_fkey FOREIGN KEY (person_email_id) REFERENCES teppelin_public.person_email(person_email_id) ON DELETE CASCADE;


--
-- Name: person_secret person_secret_person_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.person_secret
    ADD CONSTRAINT person_secret_person_id_fkey FOREIGN KEY (person_id) REFERENCES teppelin_public.person(person_id) ON DELETE CASCADE;


--
-- Name: session session_person_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_private; Owner: -
--

ALTER TABLE ONLY teppelin_private.session
    ADD CONSTRAINT session_person_id_fkey FOREIGN KEY (person_id) REFERENCES teppelin_public.person(person_id) ON DELETE CASCADE;


--
-- Name: author author_gender_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author
    ADD CONSTRAINT author_gender_id_fkey FOREIGN KEY (gender_id) REFERENCES teppelin_public.gender(gender_id) ON DELETE CASCADE;


--
-- Name: author_mapping author_mapping_author_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author_mapping
    ADD CONSTRAINT author_mapping_author_id_fkey FOREIGN KEY (author_id) REFERENCES teppelin_public.author(author_id) ON DELETE CASCADE;


--
-- Name: author_mapping author_mapping_cataloging_app_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author_mapping
    ADD CONSTRAINT author_mapping_cataloging_app_id_fkey FOREIGN KEY (cataloging_app_id) REFERENCES teppelin_public.cataloging_app(cataloging_app_id) ON DELETE CASCADE;


--
-- Name: author_name author_name_author_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author_name
    ADD CONSTRAINT author_name_author_id_fkey FOREIGN KEY (author_id) REFERENCES teppelin_public.author(author_id) ON DELETE CASCADE;


--
-- Name: author_name author_name_language_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.author_name
    ADD CONSTRAINT author_name_language_id_fkey FOREIGN KEY (language_id) REFERENCES teppelin_public.language(language_id) ON DELETE CASCADE;


--
-- Name: chapter chapter_chapter_type_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter
    ADD CONSTRAINT chapter_chapter_type_id_fkey FOREIGN KEY (chapter_type_id) REFERENCES teppelin_public.chapter_type(chapter_type_id) ON DELETE CASCADE;


--
-- Name: chapter_mapping chapter_mapping_cataloging_app_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_mapping
    ADD CONSTRAINT chapter_mapping_cataloging_app_id_fkey FOREIGN KEY (cataloging_app_id) REFERENCES teppelin_public.cataloging_app(cataloging_app_id) ON DELETE CASCADE;


--
-- Name: chapter_mapping chapter_mapping_chapter_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_mapping
    ADD CONSTRAINT chapter_mapping_chapter_id_fkey FOREIGN KEY (chapter_id) REFERENCES teppelin_public.chapter(chapter_id) ON DELETE CASCADE;


--
-- Name: chapter_thumbnail chapter_thumbnail_chapter_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_thumbnail
    ADD CONSTRAINT chapter_thumbnail_chapter_id_fkey FOREIGN KEY (chapter_id) REFERENCES teppelin_public.chapter(chapter_id) ON DELETE CASCADE;


--
-- Name: chapter_title chapter_title_chapter_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_title
    ADD CONSTRAINT chapter_title_chapter_id_fkey FOREIGN KEY (chapter_id) REFERENCES teppelin_public.chapter(chapter_id) ON DELETE CASCADE;


--
-- Name: chapter_title chapter_title_language_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter_title
    ADD CONSTRAINT chapter_title_language_id_fkey FOREIGN KEY (language_id) REFERENCES teppelin_public.language(language_id) ON DELETE CASCADE;


--
-- Name: chapter chapter_volume_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.chapter
    ADD CONSTRAINT chapter_volume_id_fkey FOREIGN KEY (volume_id) REFERENCES teppelin_public.volume(volume_id) ON DELETE CASCADE;


--
-- Name: media_author media_author_author_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_author
    ADD CONSTRAINT media_author_author_id_fkey FOREIGN KEY (author_id) REFERENCES teppelin_public.author(author_id) ON DELETE CASCADE;


--
-- Name: media_author media_author_credit_type_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_author
    ADD CONSTRAINT media_author_credit_type_id_fkey FOREIGN KEY (credit_type_id) REFERENCES teppelin_public.credit_type(credit_type_id);


--
-- Name: media_author media_author_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_author
    ADD CONSTRAINT media_author_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media media_category_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media
    ADD CONSTRAINT media_category_id_fkey FOREIGN KEY (category_id) REFERENCES teppelin_public.category(category_id) ON DELETE CASCADE;


--
-- Name: media_genre media_genre_genre_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_genre
    ADD CONSTRAINT media_genre_genre_id_fkey FOREIGN KEY (genre_id) REFERENCES teppelin_public.genre(genre_id) ON DELETE CASCADE;


--
-- Name: media_genre media_genre_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_genre
    ADD CONSTRAINT media_genre_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media media_language_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media
    ADD CONSTRAINT media_language_id_fkey FOREIGN KEY (language_id) REFERENCES teppelin_public.language(language_id) ON DELETE CASCADE;


--
-- Name: media_mapping media_mapping_cataloging_app_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_mapping
    ADD CONSTRAINT media_mapping_cataloging_app_id_fkey FOREIGN KEY (cataloging_app_id) REFERENCES teppelin_public.cataloging_app(cataloging_app_id) ON DELETE CASCADE;


--
-- Name: media_mapping media_mapping_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_mapping
    ADD CONSTRAINT media_mapping_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media media_media_state_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media
    ADD CONSTRAINT media_media_state_id_fkey FOREIGN KEY (media_state_id) REFERENCES teppelin_public.media_state(media_state_id) ON DELETE CASCADE;


--
-- Name: media media_media_type_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media
    ADD CONSTRAINT media_media_type_id_fkey FOREIGN KEY (media_type_id) REFERENCES teppelin_public.media_type(media_type_id) ON DELETE CASCADE;


--
-- Name: media_publisher media_publisher_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_publisher
    ADD CONSTRAINT media_publisher_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media_publisher media_publisher_publisher_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_publisher
    ADD CONSTRAINT media_publisher_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES teppelin_public.publisher(publisher_id) ON DELETE CASCADE;


--
-- Name: media_recommendation media_recommendation_media1_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_recommendation
    ADD CONSTRAINT media_recommendation_media1_id_fkey FOREIGN KEY (media1_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media_recommendation media_recommendation_media2_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_recommendation
    ADD CONSTRAINT media_recommendation_media2_id_fkey FOREIGN KEY (media2_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media_relation media_relation_media1_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_relation
    ADD CONSTRAINT media_relation_media1_id_fkey FOREIGN KEY (media1_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media_relation media_relation_media2_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_relation
    ADD CONSTRAINT media_relation_media2_id_fkey FOREIGN KEY (media2_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media_relation media_relation_relation_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_relation
    ADD CONSTRAINT media_relation_relation_id_fkey FOREIGN KEY (relation_id) REFERENCES teppelin_public.relation(relation_id) ON DELETE CASCADE;


--
-- Name: media_tag media_tag_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_tag
    ADD CONSTRAINT media_tag_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: media_tag media_tag_tag_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_tag
    ADD CONSTRAINT media_tag_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES teppelin_public.tag(tag_id) ON DELETE CASCADE;


--
-- Name: media_title media_title_language_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_title
    ADD CONSTRAINT media_title_language_id_fkey FOREIGN KEY (language_id) REFERENCES teppelin_public.language(language_id) ON DELETE CASCADE;


--
-- Name: media_title media_title_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.media_title
    ADD CONSTRAINT media_title_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: person_authentication person_authentication_person_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_authentication
    ADD CONSTRAINT person_authentication_person_id_fkey FOREIGN KEY (person_id) REFERENCES teppelin_public.person(person_id) ON DELETE CASCADE;


--
-- Name: person_email person_email_person_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_email
    ADD CONSTRAINT person_email_person_id_fkey FOREIGN KEY (person_id) REFERENCES teppelin_public.person(person_id) ON DELETE CASCADE;


--
-- Name: person_shelf person_shelf_cataloging_app_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_shelf
    ADD CONSTRAINT person_shelf_cataloging_app_id_fkey FOREIGN KEY (cataloging_app_id) REFERENCES teppelin_public.cataloging_app(cataloging_app_id) ON DELETE CASCADE;


--
-- Name: person_shelf person_shelf_category_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_shelf
    ADD CONSTRAINT person_shelf_category_id_fkey FOREIGN KEY (category_id) REFERENCES teppelin_public.category(category_id) ON DELETE CASCADE;


--
-- Name: person_shelf person_shelf_last_chapter_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_shelf
    ADD CONSTRAINT person_shelf_last_chapter_id_fkey FOREIGN KEY (last_chapter_id) REFERENCES teppelin_public.chapter(chapter_id) ON DELETE CASCADE;


--
-- Name: person_shelf person_shelf_person_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.person_shelf
    ADD CONSTRAINT person_shelf_person_id_fkey FOREIGN KEY (person_id) REFERENCES teppelin_public.person(person_id) ON DELETE CASCADE;


--
-- Name: poster poster_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.poster
    ADD CONSTRAINT poster_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: publisher publisher_language_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher
    ADD CONSTRAINT publisher_language_id_fkey FOREIGN KEY (language_id) REFERENCES teppelin_public.language(language_id) ON DELETE CASCADE;


--
-- Name: publisher_mapping publisher_mapping_cataloging_app_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher_mapping
    ADD CONSTRAINT publisher_mapping_cataloging_app_id_fkey FOREIGN KEY (cataloging_app_id) REFERENCES teppelin_public.cataloging_app(cataloging_app_id) ON DELETE CASCADE;


--
-- Name: publisher_mapping publisher_mapping_publisher_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher_mapping
    ADD CONSTRAINT publisher_mapping_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES teppelin_public.publisher(publisher_id) ON DELETE CASCADE;


--
-- Name: publisher_name publisher_name_language_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher_name
    ADD CONSTRAINT publisher_name_language_id_fkey FOREIGN KEY (language_id) REFERENCES teppelin_public.language(language_id) ON DELETE CASCADE;


--
-- Name: publisher_name publisher_name_publisher_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.publisher_name
    ADD CONSTRAINT publisher_name_publisher_id_fkey FOREIGN KEY (publisher_id) REFERENCES teppelin_public.publisher(publisher_id) ON DELETE CASCADE;


--
-- Name: release release_chapter_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release
    ADD CONSTRAINT release_chapter_id_fkey FOREIGN KEY (chapter_id) REFERENCES teppelin_public.chapter(chapter_id) ON DELETE CASCADE;


--
-- Name: release_group_mapping release_group_mapping_cataloging_app_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release_group_mapping
    ADD CONSTRAINT release_group_mapping_cataloging_app_id_fkey FOREIGN KEY (cataloging_app_id) REFERENCES teppelin_public.cataloging_app(cataloging_app_id) ON DELETE CASCADE;


--
-- Name: release_group_mapping release_group_mapping_release_group_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release_group_mapping
    ADD CONSTRAINT release_group_mapping_release_group_id_fkey FOREIGN KEY (release_group_id) REFERENCES teppelin_public.release_group(release_group_id) ON DELETE CASCADE;


--
-- Name: release release_language_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release
    ADD CONSTRAINT release_language_id_fkey FOREIGN KEY (language_id) REFERENCES teppelin_public.language(language_id) ON DELETE CASCADE;


--
-- Name: release_mapping release_mapping_cataloging_app_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release_mapping
    ADD CONSTRAINT release_mapping_cataloging_app_id_fkey FOREIGN KEY (cataloging_app_id) REFERENCES teppelin_public.cataloging_app(cataloging_app_id) ON DELETE CASCADE;


--
-- Name: release_mapping release_mapping_release_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release_mapping
    ADD CONSTRAINT release_mapping_release_id_fkey FOREIGN KEY (release_id) REFERENCES teppelin_public.release(release_id) ON DELETE CASCADE;


--
-- Name: release release_release_group_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.release
    ADD CONSTRAINT release_release_group_id_fkey FOREIGN KEY (release_group_id) REFERENCES teppelin_public.release_group(release_group_id) ON DELETE CASCADE;


--
-- Name: volume volume_media_id_fkey; Type: FK CONSTRAINT; Schema: teppelin_public; Owner: -
--

ALTER TABLE ONLY teppelin_public.volume
    ADD CONSTRAINT volume_media_id_fkey FOREIGN KEY (media_id) REFERENCES teppelin_public.media(media_id) ON DELETE CASCADE;


--
-- Name: connect_pg_simple_session; Type: ROW SECURITY; Schema: teppelin_private; Owner: -
--

ALTER TABLE teppelin_private.connect_pg_simple_session ENABLE ROW LEVEL SECURITY;

--
-- Name: person_authentication_secret; Type: ROW SECURITY; Schema: teppelin_private; Owner: -
--

ALTER TABLE teppelin_private.person_authentication_secret ENABLE ROW LEVEL SECURITY;

--
-- Name: person_email_secret; Type: ROW SECURITY; Schema: teppelin_private; Owner: -
--

ALTER TABLE teppelin_private.person_email_secret ENABLE ROW LEVEL SECURITY;

--
-- Name: session; Type: ROW SECURITY; Schema: teppelin_private; Owner: -
--

ALTER TABLE teppelin_private.session ENABLE ROW LEVEL SECURITY;

--
-- Name: author; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.author ENABLE ROW LEVEL SECURITY;

--
-- Name: author_mapping; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.author_mapping ENABLE ROW LEVEL SECURITY;

--
-- Name: author_name; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.author_name ENABLE ROW LEVEL SECURITY;

--
-- Name: cataloging_app; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.cataloging_app ENABLE ROW LEVEL SECURITY;

--
-- Name: category; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.category ENABLE ROW LEVEL SECURITY;

--
-- Name: chapter; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter ENABLE ROW LEVEL SECURITY;

--
-- Name: chapter_mapping; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter_mapping ENABLE ROW LEVEL SECURITY;

--
-- Name: chapter_thumbnail; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter_thumbnail ENABLE ROW LEVEL SECURITY;

--
-- Name: chapter_title; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter_title ENABLE ROW LEVEL SECURITY;

--
-- Name: chapter_type; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.chapter_type ENABLE ROW LEVEL SECURITY;

--
-- Name: credit_type; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.credit_type ENABLE ROW LEVEL SECURITY;

--
-- Name: author delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.author FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: author_name delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.author_name FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: cataloging_app delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.cataloging_app FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: category delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.category FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.chapter FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_mapping delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.chapter_mapping FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_thumbnail delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.chapter_thumbnail FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_title delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.chapter_title FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_type delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.chapter_type FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: credit_type delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.credit_type FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: genre delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.genre FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_author delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_author FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_genre delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_genre FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_publisher delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_publisher FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_recommendation delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_recommendation FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_relation delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_relation FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_state delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_state FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_tag delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_tag FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_title delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_title FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_type delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.media_type FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: person_shelf delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.person_shelf FOR DELETE USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: poster delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.poster FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: publisher delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.publisher FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: publisher_name delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.publisher_name FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: relation delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.relation FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: release delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.release FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: release_group delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.release_group FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: tag delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.tag FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: volume delete_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_admin ON teppelin_public.volume FOR DELETE USING (teppelin_public.current_person_is_admin());


--
-- Name: person_authentication delete_own; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_own ON teppelin_public.person_authentication FOR DELETE USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: person_email delete_own; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_own ON teppelin_public.person_email FOR DELETE USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: person delete_self; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY delete_self ON teppelin_public.person FOR DELETE USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: gender; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.gender ENABLE ROW LEVEL SECURITY;

--
-- Name: genre; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.genre ENABLE ROW LEVEL SECURITY;

--
-- Name: author insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.author FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: author_name insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.author_name FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: cataloging_app insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.cataloging_app FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: category insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.category FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: chapter insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.chapter FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: chapter_mapping insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.chapter_mapping FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: chapter_thumbnail insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.chapter_thumbnail FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: chapter_title insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.chapter_title FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: chapter_type insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.chapter_type FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: credit_type insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.credit_type FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: genre insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.genre FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_author insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_author FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_genre insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_genre FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_publisher insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_publisher FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_recommendation insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_recommendation FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_relation insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_relation FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_state insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_state FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_tag insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_tag FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_title insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_title FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: media_type insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.media_type FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: person_shelf insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.person_shelf FOR INSERT WITH CHECK ((person_id = teppelin_public.current_person_id()));


--
-- Name: poster insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.poster FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: publisher insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.publisher FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: publisher_name insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.publisher_name FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: relation insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.relation FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: release insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.release FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: release_group insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.release_group FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: tag insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.tag FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: volume insert_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_admin ON teppelin_public.volume FOR INSERT WITH CHECK (teppelin_public.current_person_is_admin());


--
-- Name: person_email insert_own; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY insert_own ON teppelin_public.person_email FOR INSERT WITH CHECK ((person_id = teppelin_public.current_person_id()));


--
-- Name: language; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.language ENABLE ROW LEVEL SECURITY;

--
-- Name: media; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media ENABLE ROW LEVEL SECURITY;

--
-- Name: media_author; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_author ENABLE ROW LEVEL SECURITY;

--
-- Name: media_genre; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_genre ENABLE ROW LEVEL SECURITY;

--
-- Name: media_mapping; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_mapping ENABLE ROW LEVEL SECURITY;

--
-- Name: media_publisher; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_publisher ENABLE ROW LEVEL SECURITY;

--
-- Name: media_recommendation; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_recommendation ENABLE ROW LEVEL SECURITY;

--
-- Name: media_relation; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_relation ENABLE ROW LEVEL SECURITY;

--
-- Name: media_state; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_state ENABLE ROW LEVEL SECURITY;

--
-- Name: media_tag; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_tag ENABLE ROW LEVEL SECURITY;

--
-- Name: media_title; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_title ENABLE ROW LEVEL SECURITY;

--
-- Name: media_type; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.media_type ENABLE ROW LEVEL SECURITY;

--
-- Name: person; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person ENABLE ROW LEVEL SECURITY;

--
-- Name: person_authentication; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person_authentication ENABLE ROW LEVEL SECURITY;

--
-- Name: person_email; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person_email ENABLE ROW LEVEL SECURITY;

--
-- Name: person_shelf; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.person_shelf ENABLE ROW LEVEL SECURITY;

--
-- Name: poster; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.poster ENABLE ROW LEVEL SECURITY;

--
-- Name: publisher; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.publisher ENABLE ROW LEVEL SECURITY;

--
-- Name: publisher_mapping; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.publisher_mapping ENABLE ROW LEVEL SECURITY;

--
-- Name: publisher_name; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.publisher_name ENABLE ROW LEVEL SECURITY;

--
-- Name: relation; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.relation ENABLE ROW LEVEL SECURITY;

--
-- Name: release; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.release ENABLE ROW LEVEL SECURITY;

--
-- Name: release_group; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.release_group ENABLE ROW LEVEL SECURITY;

--
-- Name: release_group_mapping; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.release_group_mapping ENABLE ROW LEVEL SECURITY;

--
-- Name: release_mapping; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.release_mapping ENABLE ROW LEVEL SECURITY;

--
-- Name: author select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.author FOR SELECT USING (true);


--
-- Name: author_mapping select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.author_mapping FOR SELECT USING (true);


--
-- Name: author_name select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.author_name FOR SELECT USING (true);


--
-- Name: cataloging_app select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.cataloging_app FOR SELECT USING (true);


--
-- Name: category select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.category FOR SELECT USING (true);


--
-- Name: chapter select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.chapter FOR SELECT USING (true);


--
-- Name: chapter_mapping select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.chapter_mapping FOR SELECT USING (true);


--
-- Name: chapter_thumbnail select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.chapter_thumbnail FOR SELECT USING (true);


--
-- Name: chapter_title select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.chapter_title FOR SELECT USING (true);


--
-- Name: chapter_type select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.chapter_type FOR SELECT USING (true);


--
-- Name: credit_type select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.credit_type FOR SELECT USING (true);


--
-- Name: gender select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.gender FOR SELECT USING (true);


--
-- Name: genre select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.genre FOR SELECT USING (true);


--
-- Name: language select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.language FOR SELECT USING (true);


--
-- Name: media select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media FOR SELECT USING (true);


--
-- Name: media_author select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_author FOR SELECT USING (true);


--
-- Name: media_genre select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_genre FOR SELECT USING (true);


--
-- Name: media_mapping select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_mapping FOR SELECT USING (true);


--
-- Name: media_publisher select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_publisher FOR SELECT USING (true);


--
-- Name: media_recommendation select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_recommendation FOR SELECT USING (true);


--
-- Name: media_relation select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_relation FOR SELECT USING (true);


--
-- Name: media_state select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_state FOR SELECT USING (true);


--
-- Name: media_tag select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_tag FOR SELECT USING (true);


--
-- Name: media_title select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_title FOR SELECT USING (true);


--
-- Name: media_type select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.media_type FOR SELECT USING (true);


--
-- Name: person select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.person FOR SELECT USING (true);


--
-- Name: person_shelf select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.person_shelf FOR SELECT USING (((public = true) OR (person_id = teppelin_public.current_person_id())));


--
-- Name: poster select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.poster FOR SELECT USING (true);


--
-- Name: publisher select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.publisher FOR SELECT USING (true);


--
-- Name: publisher_mapping select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.publisher_mapping FOR SELECT USING (true);


--
-- Name: publisher_name select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.publisher_name FOR SELECT USING (true);


--
-- Name: relation select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.relation FOR SELECT USING (true);


--
-- Name: release select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.release FOR SELECT USING (true);


--
-- Name: release_group select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.release_group FOR SELECT USING (true);


--
-- Name: release_group_mapping select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.release_group_mapping FOR SELECT USING (true);


--
-- Name: release_mapping select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.release_mapping FOR SELECT USING (true);


--
-- Name: tag select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.tag FOR SELECT USING (true);


--
-- Name: volume select_all; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_all ON teppelin_public.volume FOR SELECT USING (true);


--
-- Name: person_authentication select_own; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_own ON teppelin_public.person_authentication FOR SELECT USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: person_email select_own; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY select_own ON teppelin_public.person_email FOR SELECT USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: tag; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.tag ENABLE ROW LEVEL SECURITY;

--
-- Name: author update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.author FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: author_name update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.author_name FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: cataloging_app update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.cataloging_app FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: category update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.category FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.chapter FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_mapping update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.chapter_mapping FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_thumbnail update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.chapter_thumbnail FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_title update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.chapter_title FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: chapter_type update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.chapter_type FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: credit_type update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.credit_type FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: genre update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.genre FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_author update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_author FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_genre update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_genre FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_publisher update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_publisher FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_recommendation update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_recommendation FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_relation update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_relation FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_state update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_state FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_tag update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_tag FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_title update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_title FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: media_type update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.media_type FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: person_shelf update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.person_shelf FOR UPDATE USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: poster update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.poster FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: publisher update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.publisher FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: publisher_name update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.publisher_name FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: relation update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.relation FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: release update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.release FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: release_group update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.release_group FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: tag update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.tag FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: volume update_admin; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_admin ON teppelin_public.volume FOR UPDATE USING (teppelin_public.current_person_is_admin());


--
-- Name: person update_self; Type: POLICY; Schema: teppelin_public; Owner: -
--

CREATE POLICY update_self ON teppelin_public.person FOR UPDATE USING ((person_id = teppelin_public.current_person_id()));


--
-- Name: volume; Type: ROW SECURITY; Schema: teppelin_public; Owner: -
--

ALTER TABLE teppelin_public.volume ENABLE ROW LEVEL SECURITY;

--
-- Name: SCHEMA teppelin_public; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA teppelin_public TO teppelin_visitor;


--
-- Name: TABLE person; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.person TO teppelin_visitor;


--
-- Name: COLUMN person.username; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT UPDATE(username) ON TABLE teppelin_public.person TO teppelin_visitor;


--
-- Name: COLUMN person.display_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT UPDATE(display_name) ON TABLE teppelin_public.person TO teppelin_visitor;


--
-- Name: COLUMN person.avatar_url; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT UPDATE(avatar_url) ON TABLE teppelin_public.person TO teppelin_visitor;


--
-- Name: FUNCTION change_password(old_password text, new_password text); Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT ALL ON FUNCTION teppelin_public.change_password(old_password text, new_password text) TO teppelin_visitor;


--
-- Name: TABLE person_email; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.person_email TO teppelin_visitor;


--
-- Name: COLUMN person_email.email; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(email) ON TABLE teppelin_public.person_email TO teppelin_visitor;


--
-- Name: TABLE author; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.author TO teppelin_visitor;


--
-- Name: COLUMN author.gender_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(gender_id),UPDATE(gender_id) ON TABLE teppelin_public.author TO teppelin_visitor;


--
-- Name: COLUMN author.author_contact; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(author_contact),UPDATE(author_contact) ON TABLE teppelin_public.author TO teppelin_visitor;


--
-- Name: COLUMN author.author_note; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(author_note),UPDATE(author_note) ON TABLE teppelin_public.author TO teppelin_visitor;


--
-- Name: COLUMN author.author_photo; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(author_photo),UPDATE(author_photo) ON TABLE teppelin_public.author TO teppelin_visitor;


--
-- Name: SEQUENCE author_author_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.author_author_id_seq TO teppelin_visitor;


--
-- Name: TABLE author_mapping; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.author_mapping TO teppelin_visitor;


--
-- Name: TABLE author_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.author_name TO teppelin_visitor;


--
-- Name: COLUMN author_name.author_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(author_id) ON TABLE teppelin_public.author_name TO teppelin_visitor;


--
-- Name: COLUMN author_name.language_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(language_id),UPDATE(language_id) ON TABLE teppelin_public.author_name TO teppelin_visitor;


--
-- Name: COLUMN author_name.author_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(author_name),UPDATE(author_name) ON TABLE teppelin_public.author_name TO teppelin_visitor;


--
-- Name: COLUMN author_name.author_name_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(author_name_type),UPDATE(author_name_type) ON TABLE teppelin_public.author_name TO teppelin_visitor;


--
-- Name: SEQUENCE author_name_author_name_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.author_name_author_name_id_seq TO teppelin_visitor;


--
-- Name: TABLE cataloging_app; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.cataloging_app TO teppelin_visitor;


--
-- Name: COLUMN cataloging_app.cataloging_app; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(cataloging_app),UPDATE(cataloging_app) ON TABLE teppelin_public.cataloging_app TO teppelin_visitor;


--
-- Name: SEQUENCE cataloging_app_cataloging_app_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.cataloging_app_cataloging_app_id_seq TO teppelin_visitor;


--
-- Name: TABLE category; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.category TO teppelin_visitor;


--
-- Name: COLUMN category.category; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(category),UPDATE(category) ON TABLE teppelin_public.category TO teppelin_visitor;


--
-- Name: SEQUENCE category_category_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.category_category_id_seq TO teppelin_visitor;


--
-- Name: TABLE chapter; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.chapter TO teppelin_visitor;


--
-- Name: COLUMN chapter.chapter_type_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_type_id),UPDATE(chapter_type_id) ON TABLE teppelin_public.chapter TO teppelin_visitor;


--
-- Name: COLUMN chapter.volume_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(volume_id),UPDATE(volume_id) ON TABLE teppelin_public.chapter TO teppelin_visitor;


--
-- Name: COLUMN chapter.chapter; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter),UPDATE(chapter) ON TABLE teppelin_public.chapter TO teppelin_visitor;


--
-- Name: COLUMN chapter.chapter_length; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_length),UPDATE(chapter_length) ON TABLE teppelin_public.chapter TO teppelin_visitor;


--
-- Name: COLUMN chapter.release_date; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(release_date),UPDATE(release_date) ON TABLE teppelin_public.chapter TO teppelin_visitor;


--
-- Name: SEQUENCE chapter_chapter_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.chapter_chapter_id_seq TO teppelin_visitor;


--
-- Name: TABLE chapter_mapping; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.chapter_mapping TO teppelin_visitor;


--
-- Name: COLUMN chapter_mapping.chapter_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_id),UPDATE(chapter_id) ON TABLE teppelin_public.chapter_mapping TO teppelin_visitor;


--
-- Name: COLUMN chapter_mapping.cataloging_app_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(cataloging_app_id),UPDATE(cataloging_app_id) ON TABLE teppelin_public.chapter_mapping TO teppelin_visitor;


--
-- Name: COLUMN chapter_mapping.foreign_media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(foreign_media_id),UPDATE(foreign_media_id) ON TABLE teppelin_public.chapter_mapping TO teppelin_visitor;


--
-- Name: TABLE chapter_thumbnail; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.chapter_thumbnail TO teppelin_visitor;


--
-- Name: COLUMN chapter_thumbnail.chapter_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_id) ON TABLE teppelin_public.chapter_thumbnail TO teppelin_visitor;


--
-- Name: COLUMN chapter_thumbnail.thumbnail_url; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(thumbnail_url),UPDATE(thumbnail_url) ON TABLE teppelin_public.chapter_thumbnail TO teppelin_visitor;


--
-- Name: SEQUENCE chapter_thumbnail_chapter_thumbnail_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.chapter_thumbnail_chapter_thumbnail_id_seq TO teppelin_visitor;


--
-- Name: TABLE chapter_title; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.chapter_title TO teppelin_visitor;


--
-- Name: COLUMN chapter_title.chapter_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_id),UPDATE(chapter_id) ON TABLE teppelin_public.chapter_title TO teppelin_visitor;


--
-- Name: COLUMN chapter_title.language_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(language_id),UPDATE(language_id) ON TABLE teppelin_public.chapter_title TO teppelin_visitor;


--
-- Name: COLUMN chapter_title.chapter_title; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_title),UPDATE(chapter_title) ON TABLE teppelin_public.chapter_title TO teppelin_visitor;


--
-- Name: SEQUENCE chapter_title_chapter_title_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.chapter_title_chapter_title_id_seq TO teppelin_visitor;


--
-- Name: TABLE chapter_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.chapter_type TO teppelin_visitor;


--
-- Name: COLUMN chapter_type.chapter_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_type),UPDATE(chapter_type) ON TABLE teppelin_public.chapter_type TO teppelin_visitor;


--
-- Name: SEQUENCE chapter_type_chapter_type_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.chapter_type_chapter_type_id_seq TO teppelin_visitor;


--
-- Name: TABLE credit_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.credit_type TO teppelin_visitor;


--
-- Name: COLUMN credit_type.credit_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(credit_type),UPDATE(credit_type) ON TABLE teppelin_public.credit_type TO teppelin_visitor;


--
-- Name: COLUMN credit_type.details; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(details),UPDATE(details) ON TABLE teppelin_public.credit_type TO teppelin_visitor;


--
-- Name: SEQUENCE credit_type_credit_type_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.credit_type_credit_type_id_seq TO teppelin_visitor;


--
-- Name: TABLE gender; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.gender TO teppelin_visitor;


--
-- Name: TABLE genre; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.genre TO teppelin_visitor;


--
-- Name: COLUMN genre.genre; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(genre),UPDATE(genre) ON TABLE teppelin_public.genre TO teppelin_visitor;


--
-- Name: COLUMN genre.genre_description; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(genre_description),UPDATE(genre_description) ON TABLE teppelin_public.genre TO teppelin_visitor;


--
-- Name: SEQUENCE genre_genre_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.genre_genre_id_seq TO teppelin_visitor;


--
-- Name: TABLE language; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.language TO teppelin_visitor;


--
-- Name: SEQUENCE language_language_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.language_language_id_seq TO teppelin_visitor;


--
-- Name: TABLE media; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.category_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(category_id),UPDATE(category_id) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.media_state_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_state_id),UPDATE(media_state_id) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.media_type_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_type_id),UPDATE(media_type_id) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.status_in_coo; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(status_in_coo),UPDATE(status_in_coo) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.synopsis; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(synopsis),UPDATE(synopsis) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.start_date; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(start_date),UPDATE(start_date) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.end_date; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(end_date),UPDATE(end_date) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.chapter_count; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_count),UPDATE(chapter_count) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.weighted_rating; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(weighted_rating),UPDATE(weighted_rating) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.average_rating; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(average_rating),UPDATE(average_rating) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.licensed; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(licensed),UPDATE(licensed) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.completely_translated; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(completely_translated),UPDATE(completely_translated) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: COLUMN media.nsfw; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(nsfw),UPDATE(nsfw) ON TABLE teppelin_public.media TO teppelin_visitor;


--
-- Name: TABLE media_author; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_author TO teppelin_visitor;


--
-- Name: COLUMN media_author.media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_id),UPDATE(media_id) ON TABLE teppelin_public.media_author TO teppelin_visitor;


--
-- Name: COLUMN media_author.author_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(author_id),UPDATE(author_id) ON TABLE teppelin_public.media_author TO teppelin_visitor;


--
-- Name: TABLE media_genre; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_genre TO teppelin_visitor;


--
-- Name: COLUMN media_genre.media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_id),UPDATE(media_id) ON TABLE teppelin_public.media_genre TO teppelin_visitor;


--
-- Name: COLUMN media_genre.genre_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(genre_id),UPDATE(genre_id) ON TABLE teppelin_public.media_genre TO teppelin_visitor;


--
-- Name: TABLE media_mapping; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.media_mapping TO teppelin_visitor;


--
-- Name: SEQUENCE media_media_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.media_media_id_seq TO teppelin_visitor;


--
-- Name: TABLE media_publisher; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_publisher TO teppelin_visitor;


--
-- Name: COLUMN media_publisher.media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_id),UPDATE(media_id) ON TABLE teppelin_public.media_publisher TO teppelin_visitor;


--
-- Name: COLUMN media_publisher.publisher_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(publisher_id),UPDATE(publisher_id) ON TABLE teppelin_public.media_publisher TO teppelin_visitor;


--
-- Name: TABLE media_recommendation; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_recommendation TO teppelin_visitor;


--
-- Name: COLUMN media_recommendation.media1_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media1_id) ON TABLE teppelin_public.media_recommendation TO teppelin_visitor;


--
-- Name: COLUMN media_recommendation.media2_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media2_id) ON TABLE teppelin_public.media_recommendation TO teppelin_visitor;


--
-- Name: COLUMN media_recommendation.approvals; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(approvals),UPDATE(approvals) ON TABLE teppelin_public.media_recommendation TO teppelin_visitor;


--
-- Name: TABLE media_relation; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_relation TO teppelin_visitor;


--
-- Name: COLUMN media_relation.media1_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media1_id) ON TABLE teppelin_public.media_relation TO teppelin_visitor;


--
-- Name: COLUMN media_relation.media2_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media2_id) ON TABLE teppelin_public.media_relation TO teppelin_visitor;


--
-- Name: COLUMN media_relation.relation_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(relation_id),UPDATE(relation_id) ON TABLE teppelin_public.media_relation TO teppelin_visitor;


--
-- Name: TABLE media_state; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_state TO teppelin_visitor;


--
-- Name: COLUMN media_state.media_state; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_state),UPDATE(media_state) ON TABLE teppelin_public.media_state TO teppelin_visitor;


--
-- Name: SEQUENCE media_state_media_state_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.media_state_media_state_id_seq TO teppelin_visitor;


--
-- Name: TABLE media_tag; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_tag TO teppelin_visitor;


--
-- Name: COLUMN media_tag.media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_id),UPDATE(media_id) ON TABLE teppelin_public.media_tag TO teppelin_visitor;


--
-- Name: COLUMN media_tag.tag_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(tag_id),UPDATE(tag_id) ON TABLE teppelin_public.media_tag TO teppelin_visitor;


--
-- Name: TABLE media_title; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_title TO teppelin_visitor;


--
-- Name: COLUMN media_title.media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_id),UPDATE(media_id) ON TABLE teppelin_public.media_title TO teppelin_visitor;


--
-- Name: COLUMN media_title.media_title; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_title),UPDATE(media_title) ON TABLE teppelin_public.media_title TO teppelin_visitor;


--
-- Name: COLUMN media_title.language_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(language_id),UPDATE(language_id) ON TABLE teppelin_public.media_title TO teppelin_visitor;


--
-- Name: COLUMN media_title.media_name_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_name_type),UPDATE(media_name_type) ON TABLE teppelin_public.media_title TO teppelin_visitor;


--
-- Name: SEQUENCE media_title_media_title_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.media_title_media_title_id_seq TO teppelin_visitor;


--
-- Name: TABLE media_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.media_type TO teppelin_visitor;


--
-- Name: COLUMN media_type.media_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_type),UPDATE(media_type) ON TABLE teppelin_public.media_type TO teppelin_visitor;


--
-- Name: SEQUENCE media_type_media_type_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.media_type_media_type_id_seq TO teppelin_visitor;


--
-- Name: TABLE person_authentication; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.person_authentication TO teppelin_visitor;


--
-- Name: SEQUENCE person_authentication_person_authentication_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.person_authentication_person_authentication_id_seq TO teppelin_visitor;


--
-- Name: SEQUENCE person_email_person_email_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.person_email_person_email_id_seq TO teppelin_visitor;


--
-- Name: SEQUENCE person_person_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.person_person_id_seq TO teppelin_visitor;


--
-- Name: TABLE person_shelf; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: COLUMN person_shelf.category_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(category_id) ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: COLUMN person_shelf.last_chapter_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(last_chapter_id),UPDATE(last_chapter_id) ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: COLUMN person_shelf.shelf_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(shelf_name),UPDATE(shelf_name) ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: COLUMN person_shelf.public; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(public),UPDATE(public) ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: COLUMN person_shelf.cataloging_app_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(cataloging_app_id),UPDATE(cataloging_app_id) ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: COLUMN person_shelf.shelf_watch_state; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(shelf_watch_state),UPDATE(shelf_watch_state) ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: COLUMN person_shelf.sync_shelf; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(sync_shelf),UPDATE(sync_shelf) ON TABLE teppelin_public.person_shelf TO teppelin_visitor;


--
-- Name: SEQUENCE person_shelf_person_shelf_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.person_shelf_person_shelf_id_seq TO teppelin_visitor;


--
-- Name: TABLE poster; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.poster TO teppelin_visitor;


--
-- Name: COLUMN poster.media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_id) ON TABLE teppelin_public.poster TO teppelin_visitor;


--
-- Name: COLUMN poster.main; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(main),UPDATE(main) ON TABLE teppelin_public.poster TO teppelin_visitor;


--
-- Name: COLUMN poster.poster_url; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(poster_url),UPDATE(poster_url) ON TABLE teppelin_public.poster TO teppelin_visitor;


--
-- Name: SEQUENCE poster_poster_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.poster_poster_id_seq TO teppelin_visitor;


--
-- Name: TABLE publisher; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.publisher TO teppelin_visitor;


--
-- Name: COLUMN publisher.language_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(language_id),UPDATE(language_id) ON TABLE teppelin_public.publisher TO teppelin_visitor;


--
-- Name: COLUMN publisher.publisher_contact; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(publisher_contact),UPDATE(publisher_contact) ON TABLE teppelin_public.publisher TO teppelin_visitor;


--
-- Name: COLUMN publisher.publisher_note; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(publisher_note),UPDATE(publisher_note) ON TABLE teppelin_public.publisher TO teppelin_visitor;


--
-- Name: TABLE publisher_mapping; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.publisher_mapping TO teppelin_visitor;


--
-- Name: TABLE publisher_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.publisher_name TO teppelin_visitor;


--
-- Name: COLUMN publisher_name.publisher_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(publisher_id) ON TABLE teppelin_public.publisher_name TO teppelin_visitor;


--
-- Name: COLUMN publisher_name.language_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(language_id),UPDATE(language_id) ON TABLE teppelin_public.publisher_name TO teppelin_visitor;


--
-- Name: COLUMN publisher_name.publisher_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(publisher_name),UPDATE(publisher_name) ON TABLE teppelin_public.publisher_name TO teppelin_visitor;


--
-- Name: COLUMN publisher_name.publisher_name_type; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(publisher_name_type),UPDATE(publisher_name_type) ON TABLE teppelin_public.publisher_name TO teppelin_visitor;


--
-- Name: SEQUENCE publisher_name_publisher_name_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.publisher_name_publisher_name_id_seq TO teppelin_visitor;


--
-- Name: SEQUENCE publisher_publisher_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.publisher_publisher_id_seq TO teppelin_visitor;


--
-- Name: TABLE relation; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.relation TO teppelin_visitor;


--
-- Name: COLUMN relation.relation; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(relation),UPDATE(relation) ON TABLE teppelin_public.relation TO teppelin_visitor;


--
-- Name: SEQUENCE relation_relation_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.relation_relation_id_seq TO teppelin_visitor;


--
-- Name: TABLE release; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.release TO teppelin_visitor;


--
-- Name: COLUMN release.chapter_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(chapter_id) ON TABLE teppelin_public.release TO teppelin_visitor;


--
-- Name: COLUMN release.release_group_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(release_group_id),UPDATE(release_group_id) ON TABLE teppelin_public.release TO teppelin_visitor;


--
-- Name: COLUMN release.language_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(language_id),UPDATE(language_id) ON TABLE teppelin_public.release TO teppelin_visitor;


--
-- Name: COLUMN release.content; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(content),UPDATE(content) ON TABLE teppelin_public.release TO teppelin_visitor;


--
-- Name: COLUMN release.source_url; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(source_url),UPDATE(source_url) ON TABLE teppelin_public.release TO teppelin_visitor;


--
-- Name: COLUMN release.translator; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(translator),UPDATE(translator) ON TABLE teppelin_public.release TO teppelin_visitor;


--
-- Name: TABLE release_group; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.release_group TO teppelin_visitor;


--
-- Name: COLUMN release_group.release_group_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(release_group_name),UPDATE(release_group_name) ON TABLE teppelin_public.release_group TO teppelin_visitor;


--
-- Name: COLUMN release_group.release_group_contact; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(release_group_contact),UPDATE(release_group_contact) ON TABLE teppelin_public.release_group TO teppelin_visitor;


--
-- Name: TABLE release_group_mapping; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.release_group_mapping TO teppelin_visitor;


--
-- Name: SEQUENCE release_group_release_group_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.release_group_release_group_id_seq TO teppelin_visitor;


--
-- Name: TABLE release_mapping; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT ON TABLE teppelin_public.release_mapping TO teppelin_visitor;


--
-- Name: SEQUENCE release_release_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.release_release_id_seq TO teppelin_visitor;


--
-- Name: TABLE tag; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.tag TO teppelin_visitor;


--
-- Name: COLUMN tag.tag; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(tag),UPDATE(tag) ON TABLE teppelin_public.tag TO teppelin_visitor;


--
-- Name: COLUMN tag.tag_description; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(tag_description),UPDATE(tag_description) ON TABLE teppelin_public.tag TO teppelin_visitor;


--
-- Name: SEQUENCE tag_tag_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.tag_tag_id_seq TO teppelin_visitor;


--
-- Name: TABLE volume; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,DELETE ON TABLE teppelin_public.volume TO teppelin_visitor;


--
-- Name: COLUMN volume.media_id; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(media_id),UPDATE(media_id) ON TABLE teppelin_public.volume TO teppelin_visitor;


--
-- Name: COLUMN volume.volume_number; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(volume_number),UPDATE(volume_number) ON TABLE teppelin_public.volume TO teppelin_visitor;


--
-- Name: COLUMN volume.volume_name; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT INSERT(volume_name),UPDATE(volume_name) ON TABLE teppelin_public.volume TO teppelin_visitor;


--
-- Name: SEQUENCE volume_volume_id_seq; Type: ACL; Schema: teppelin_public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE teppelin_public.volume_volume_id_seq TO teppelin_visitor;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: teppelin_public; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA teppelin_public REVOKE ALL ON SEQUENCES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA teppelin_public GRANT SELECT,USAGE ON SEQUENCES  TO teppelin_visitor;


--
-- PostgreSQL database dump complete
--

