#!/usr/bin/env bash
set -e

echo "shared_preload_libraries = 'pg_cron'" >> $PGDATA/postgresql.conf
echo "cron.database_name = 'teppelin'" >> $PGDATA/postgresql.conf

pg_ctl restart

psql -X -v ON_ERROR_STOP=1 template1 <<SQL
drop role if exists teppelin_authenticator;
create role teppelin_authenticator with login password '${AUTH_USER_PASSWORD}' noinherit;
SQL

echo "Auth role created, now sourcing the initial database schema"
