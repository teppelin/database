-- RESET database
drop role if exists teppelin_visitor;
drop role if exists teppelin_admin;

-- Ref: https://devcenter.heroku.com/articles/heroku-postgresql#connection-permissions
-- This is the role that PostGraphile will switch to (from teppelin_authenticator) during a transaction
create role teppelin_visitor;

-- This enables PostGraphile to switch from teppelin_authenticator to teppelin_visitor
grant teppelin_visitor to teppelin_authenticator;

grant connect on database teppelin to teppelin_authenticator;
-- grant all on database teppelin to teppelin;

-- Some extensions require superuser privileges, so we create them before migration time.
create extension if not exists pg_trgm;
create extension if not exists plpgsql with schema pg_catalog;
create extension if not exists "uuid-ossp" with schema public;
create extension if not exists citext;
create extension if not exists pgcrypto;
create extension if not exists pg_cron;
-- create extension if not exists amqp;
-- create extension if not exists pllua;
