package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/studio-b12/gowebdav"
)

func checkErr(err error) {
	if err != nil {
		log.Fatal("ERROR:", err)
	}
}

func main() {
	if len(os.Args) < 5 {
		log.Fatal(("4 parameters required: [root dav URL] [dump directory] [username] [password] [database url]"))
	}

	root := os.Args[1]
	dir := os.Args[2]
	user := os.Args[3]
	password := os.Args[4]
	databaseURL := os.Args[5]

	c := gowebdav.NewClient(root, user, password)

	files, err := c.ReadDir(dir)
	checkErr(err)

	latestDump := files[len(files)-1]

	log.Printf("Downloading dump %s", (latestDump.Name()))

	webdavFilePath := dir + latestDump.Name()
	localFilePath := "dump.sql.gz"

	fileBytes, err := c.Read(webdavFilePath)
	checkErr(err)

	err = ioutil.WriteFile(localFilePath, fileBytes, 0644)
	checkErr(err)

	log.Println("Success, restoring dump..")

	gzip := exec.Command("gunzip", "-c", localFilePath)
	psql := exec.Command("psql", databaseURL, "-a")
	psql.Stdin, _ = gzip.StdoutPipe()

	var out, stderr bytes.Buffer
	psql.Stdout = &out
	psql.Stderr = &stderr

	psql.Start()
	gzip.Run()
	psql.Wait()
	if err != nil {
		log.Fatalf("Error executing query. Command Output: %+v\n: %+v, %v", out.String(), stderr.String(), err)
	}
}
