--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.3 (Debian 11.3-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: language; Type: TABLE DATA; Schema: teppelin_public; Owner: postgres
--

INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (1, 'aa', 'aa', 'aar', 'Afar', '2019-06-10 22:51:16.047692+00', '2019-06-10 22:51:16.047692+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (2, 'ab', 'ab', 'abk', 'Abkhazian', '2019-06-10 22:51:16.058113+00', '2019-06-10 22:51:16.058113+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (3, 'af', 'af', 'afr', 'Afrikaans', '2019-06-10 22:51:16.05985+00', '2019-06-10 22:51:16.05985+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (4, 'ak', 'ak', 'aka', 'Akan', '2019-06-10 22:51:16.061159+00', '2019-06-10 22:51:16.061159+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (5, 'sq', 'sq', 'alb', 'Albanian', '2019-06-10 22:51:16.062645+00', '2019-06-10 22:51:16.062645+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (6, 'am', 'am', 'amh', 'Amharic', '2019-06-10 22:51:16.064715+00', '2019-06-10 22:51:16.064715+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (7, 'ar', 'ar', 'ara', 'Arabic', '2019-06-10 22:51:16.067994+00', '2019-06-10 22:51:16.067994+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (8, 'an', 'an', 'arg', 'Aragonese', '2019-06-10 22:51:16.071233+00', '2019-06-10 22:51:16.071233+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (9, 'hy', 'hy', 'arm', 'Armenian', '2019-06-10 22:51:16.074588+00', '2019-06-10 22:51:16.074588+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (10, 'as', 'as', 'asm', 'Assamese', '2019-06-10 22:51:16.07776+00', '2019-06-10 22:51:16.07776+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (11, 'av', 'av', 'ava', 'Avaric', '2019-06-10 22:51:16.081083+00', '2019-06-10 22:51:16.081083+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (12, 'ae', 'ae', 'ave', 'Avestan', '2019-06-10 22:51:16.084643+00', '2019-06-10 22:51:16.084643+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (13, 'ay', 'ay', 'aym', 'Aymara', '2019-06-10 22:51:16.088012+00', '2019-06-10 22:51:16.088012+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (14, 'az', 'az', 'aze', 'Azerbaijani', '2019-06-10 22:51:16.09123+00', '2019-06-10 22:51:16.09123+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (15, 'ba', 'ba', 'bak', 'Bashkir', '2019-06-10 22:51:16.094932+00', '2019-06-10 22:51:16.094932+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (16, 'bm', 'bm', 'bam', 'Bambara', '2019-06-10 22:51:16.098203+00', '2019-06-10 22:51:16.098203+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (17, 'eu', 'eu', 'baq', 'Basque', '2019-06-10 22:51:16.1019+00', '2019-06-10 22:51:16.1019+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (18, 'be', 'be', 'bel', 'Belarusian', '2019-06-10 22:51:16.1055+00', '2019-06-10 22:51:16.1055+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (19, 'bn', 'bn', 'ben', 'Bengali', '2019-06-10 22:51:16.108692+00', '2019-06-10 22:51:16.108692+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (20, 'bh', 'bh', 'bih', 'Bihari languages', '2019-06-10 22:51:16.111763+00', '2019-06-10 22:51:16.111763+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (21, 'bi', 'bi', 'bis', 'Bislama', '2019-06-10 22:51:16.114731+00', '2019-06-10 22:51:16.114731+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (22, 'bs', 'bs', 'bos', 'Bosnian', '2019-06-10 22:51:16.118219+00', '2019-06-10 22:51:16.118219+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (23, 'br', 'br', 'bre', 'Breton', '2019-06-10 22:51:16.121587+00', '2019-06-10 22:51:16.121587+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (24, 'bg', 'bg', 'bul', 'Bulgarian', '2019-06-10 22:51:16.124946+00', '2019-06-10 22:51:16.124946+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (25, 'my', 'my', 'bur', 'Burmese', '2019-06-10 22:51:16.131642+00', '2019-06-10 22:51:16.131642+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (26, 'ca', 'ca', 'cat', 'Catalan; Valencian', '2019-06-10 22:51:16.135085+00', '2019-06-10 22:51:16.135085+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (27, 'ch', 'ch', 'cha', 'Chamorro', '2019-06-10 22:51:16.138589+00', '2019-06-10 22:51:16.138589+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (28, 'ce', 'ce', 'che', 'Chechen', '2019-06-10 22:51:16.142138+00', '2019-06-10 22:51:16.142138+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (29, 'zh', 'zh', 'chi', 'Chinese', '2019-06-10 22:51:16.145608+00', '2019-06-10 22:51:16.145608+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (30, 'cu', 'cu', 'chu', 'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic', '2019-06-10 22:51:16.148957+00', '2019-06-10 22:51:16.148957+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (31, 'cv', 'cv', 'chv', 'Chuvash', '2019-06-10 22:51:16.152029+00', '2019-06-10 22:51:16.152029+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (32, 'kw', 'kw', 'cor', 'Cornish', '2019-06-10 22:51:16.154974+00', '2019-06-10 22:51:16.154974+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (33, 'co', 'co', 'cos', 'Corsican', '2019-06-10 22:51:16.15779+00', '2019-06-10 22:51:16.15779+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (34, 'cr', 'cr', 'cre', 'Cree', '2019-06-10 22:51:16.160659+00', '2019-06-10 22:51:16.160659+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (35, 'cs', 'cs', 'cze', 'Czech', '2019-06-10 22:51:16.162938+00', '2019-06-10 22:51:16.162938+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (36, 'da', 'da', 'dan', 'Danish', '2019-06-10 22:51:16.164797+00', '2019-06-10 22:51:16.164797+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (37, 'dv', 'dv', 'div', 'Divehi; Dhivehi; Maldivian', '2019-06-10 22:51:16.166463+00', '2019-06-10 22:51:16.166463+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (38, 'nl', 'nl', 'dut', 'Dutch; Flemish', '2019-06-10 22:51:16.168249+00', '2019-06-10 22:51:16.168249+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (39, 'dz', 'dz', 'dzo', 'Dzongkha', '2019-06-10 22:51:16.170323+00', '2019-06-10 22:51:16.170323+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (40, 'en', 'en', 'eng', 'English', '2019-06-10 22:51:16.172491+00', '2019-06-10 22:51:16.172491+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (41, 'eo', 'eo', 'epo', 'Esperanto', '2019-06-10 22:51:16.174564+00', '2019-06-10 22:51:16.174564+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (42, 'et', 'et', 'est', 'Estonian', '2019-06-10 22:51:16.176532+00', '2019-06-10 22:51:16.176532+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (43, 'ee', 'ee', 'ewe', 'Ewe', '2019-06-10 22:51:16.178261+00', '2019-06-10 22:51:16.178261+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (44, 'fo', 'fo', 'fao', 'Faroese', '2019-06-10 22:51:16.179891+00', '2019-06-10 22:51:16.179891+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (45, 'fj', 'fj', 'fij', 'Fijian', '2019-06-10 22:51:16.181514+00', '2019-06-10 22:51:16.181514+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (46, 'fi', 'fi', 'fin', 'Finnish', '2019-06-10 22:51:16.183286+00', '2019-06-10 22:51:16.183286+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (47, 'fr', 'fr', 'fre', 'French', '2019-06-10 22:51:16.184894+00', '2019-06-10 22:51:16.184894+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (48, 'fy', 'fy', 'fry', 'Western Frisian', '2019-06-10 22:51:16.18674+00', '2019-06-10 22:51:16.18674+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (49, 'ff', 'ff', 'ful', 'Fulah', '2019-06-10 22:51:16.188576+00', '2019-06-10 22:51:16.188576+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (50, 'ka', 'ka', 'geo', 'Georgian', '2019-06-10 22:51:16.190568+00', '2019-06-10 22:51:16.190568+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (51, 'de', 'de', 'ger', 'German', '2019-06-10 22:51:16.19249+00', '2019-06-10 22:51:16.19249+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (52, 'gd', 'gd', 'gla', 'Gaelic; Scottish Gaelic', '2019-06-10 22:51:16.198216+00', '2019-06-10 22:51:16.198216+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (53, 'ga', 'ga', 'gle', 'Irish', '2019-06-10 22:51:16.20146+00', '2019-06-10 22:51:16.20146+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (54, 'gl', 'gl', 'glg', 'Galician', '2019-06-10 22:51:16.204841+00', '2019-06-10 22:51:16.204841+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (55, 'gv', 'gv', 'glv', 'Manx', '2019-06-10 22:51:16.208101+00', '2019-06-10 22:51:16.208101+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (56, 'el', 'el', 'gre', 'Greek, Modern (1453-)', '2019-06-10 22:51:16.21145+00', '2019-06-10 22:51:16.21145+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (57, 'gn', 'gn', 'grn', 'Guarani', '2019-06-10 22:51:16.214993+00', '2019-06-10 22:51:16.214993+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (58, 'gu', 'gu', 'guj', 'Gujarati', '2019-06-10 22:51:16.218406+00', '2019-06-10 22:51:16.218406+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (59, 'ht', 'ht', 'hat', 'Haitian; Haitian Creole', '2019-06-10 22:51:16.221718+00', '2019-06-10 22:51:16.221718+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (60, 'ha', 'ha', 'hau', 'Hausa', '2019-06-10 22:51:16.225122+00', '2019-06-10 22:51:16.225122+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (61, 'he', 'he', 'heb', 'Hebrew', '2019-06-10 22:51:16.229016+00', '2019-06-10 22:51:16.229016+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (62, 'hz', 'hz', 'her', 'Herero', '2019-06-10 22:51:16.232633+00', '2019-06-10 22:51:16.232633+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (63, 'hi', 'hi', 'hin', 'Hindi', '2019-06-10 22:51:16.236391+00', '2019-06-10 22:51:16.236391+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (64, 'ho', 'ho', 'hmo', 'Hiri Motu', '2019-06-10 22:51:16.240346+00', '2019-06-10 22:51:16.240346+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (65, 'hr', 'hr', 'hrv', 'Croatian', '2019-06-10 22:51:16.243882+00', '2019-06-10 22:51:16.243882+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (66, 'hu', 'hu', 'hun', 'Hungarian', '2019-06-10 22:51:16.247697+00', '2019-06-10 22:51:16.247697+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (67, 'ig', 'ig', 'ibo', 'Igbo', '2019-06-10 22:51:16.251112+00', '2019-06-10 22:51:16.251112+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (68, 'is', 'is', 'ice', 'Icelandic', '2019-06-10 22:51:16.254313+00', '2019-06-10 22:51:16.254313+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (69, 'io', 'io', 'ido', 'Ido', '2019-06-10 22:51:16.257524+00', '2019-06-10 22:51:16.257524+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (70, 'ii', 'ii', 'iii', 'Sichuan Yi; Nuosu', '2019-06-10 22:51:16.260869+00', '2019-06-10 22:51:16.260869+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (71, 'iu', 'iu', 'iku', 'Inuktitut', '2019-06-10 22:51:16.264137+00', '2019-06-10 22:51:16.264137+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (72, 'ie', 'ie', 'ile', 'Interlingue; Occidental', '2019-06-10 22:51:16.267288+00', '2019-06-10 22:51:16.267288+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (73, 'ia', 'ia', 'ina', 'Interlingua (International Auxiliary Language Association)', '2019-06-10 22:51:16.271031+00', '2019-06-10 22:51:16.271031+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (74, 'id', 'id', 'ind', 'Indonesian', '2019-06-10 22:51:16.27445+00', '2019-06-10 22:51:16.27445+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (75, 'ik', 'ik', 'ipk', 'Inupiaq', '2019-06-10 22:51:16.277715+00', '2019-06-10 22:51:16.277715+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (76, 'it', 'it', 'ita', 'Italian', '2019-06-10 22:51:16.280974+00', '2019-06-10 22:51:16.280974+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (77, 'jv', 'jv', 'jav', 'Javanese', '2019-06-10 22:51:16.284381+00', '2019-06-10 22:51:16.284381+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (78, 'ja', 'ja', 'jpn', 'Japanese', '2019-06-10 22:51:16.287737+00', '2019-06-10 22:51:16.287737+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (79, 'kl', 'kl', 'kal', 'Kalaallisut; Greenlandic', '2019-06-10 22:51:16.291066+00', '2019-06-10 22:51:16.291066+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (80, 'kn', 'kn', 'kan', 'Kannada', '2019-06-10 22:51:16.294349+00', '2019-06-10 22:51:16.294349+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (81, 'ks', 'ks', 'kas', 'Kashmiri', '2019-06-10 22:51:16.297631+00', '2019-06-10 22:51:16.297631+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (82, 'kr', 'kr', 'kau', 'Kanuri', '2019-06-10 22:51:16.300872+00', '2019-06-10 22:51:16.300872+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (83, 'kk', 'kk', 'kaz', 'Kazakh', '2019-06-10 22:51:16.304192+00', '2019-06-10 22:51:16.304192+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (84, 'km', 'km', 'khm', 'Central Khmer', '2019-06-10 22:51:16.307633+00', '2019-06-10 22:51:16.307633+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (85, 'ki', 'ki', 'kik', 'Kikuyu; Gikuyu', '2019-06-10 22:51:16.311076+00', '2019-06-10 22:51:16.311076+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (86, 'rw', 'rw', 'kin', 'Kinyarwanda', '2019-06-10 22:51:16.314331+00', '2019-06-10 22:51:16.314331+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (87, 'ky', 'ky', 'kir', 'Kirghiz; Kyrgyz', '2019-06-10 22:51:16.317759+00', '2019-06-10 22:51:16.317759+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (88, 'kv', 'kv', 'kom', 'Komi', '2019-06-10 22:51:16.3214+00', '2019-06-10 22:51:16.3214+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (89, 'kg', 'kg', 'kon', 'Kongo', '2019-06-10 22:51:16.324732+00', '2019-06-10 22:51:16.324732+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (90, 'ko', 'ko', 'kor', 'Korean', '2019-06-10 22:51:16.328093+00', '2019-06-10 22:51:16.328093+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (91, 'kj', 'kj', 'kua', 'Kuanyama; Kwanyama', '2019-06-10 22:51:16.331484+00', '2019-06-10 22:51:16.331484+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (92, 'ku', 'ku', 'kur', 'Kurdish', '2019-06-10 22:51:16.334916+00', '2019-06-10 22:51:16.334916+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (93, 'lo', 'lo', 'lao', 'Lao', '2019-06-10 22:51:16.33824+00', '2019-06-10 22:51:16.33824+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (94, 'la', 'la', 'lat', 'Latin', '2019-06-10 22:51:16.341555+00', '2019-06-10 22:51:16.341555+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (95, 'lv', 'lv', 'lav', 'Latvian', '2019-06-10 22:51:16.348803+00', '2019-06-10 22:51:16.348803+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (96, 'li', 'li', 'lim', 'Limburgan; Limburger; Limburgish', '2019-06-10 22:51:16.352849+00', '2019-06-10 22:51:16.352849+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (97, 'ln', 'ln', 'lin', 'Lingala', '2019-06-10 22:51:16.358155+00', '2019-06-10 22:51:16.358155+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (98, 'lt', 'lt', 'lit', 'Lithuanian', '2019-06-10 22:51:16.361333+00', '2019-06-10 22:51:16.361333+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (99, 'lb', 'lb', 'ltz', 'Luxembourgish; Letzeburgesch', '2019-06-10 22:51:16.364446+00', '2019-06-10 22:51:16.364446+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (100, 'lu', 'lu', 'lub', 'Luba-Katanga', '2019-06-10 22:51:16.367753+00', '2019-06-10 22:51:16.367753+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (101, 'lg', 'lg', 'lug', 'Ganda', '2019-06-10 22:51:16.371073+00', '2019-06-10 22:51:16.371073+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (102, 'mk', 'mk', 'mac', 'Macedonian', '2019-06-10 22:51:16.374376+00', '2019-06-10 22:51:16.374376+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (103, 'mh', 'mh', 'mah', 'Marshallese', '2019-06-10 22:51:16.377524+00', '2019-06-10 22:51:16.377524+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (104, 'ml', 'ml', 'mal', 'Malayalam', '2019-06-10 22:51:16.381331+00', '2019-06-10 22:51:16.381331+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (105, 'mi', 'mi', 'mao', 'Maori', '2019-06-10 22:51:16.384475+00', '2019-06-10 22:51:16.384475+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (106, 'mr', 'mr', 'mar', 'Marathi', '2019-06-10 22:51:16.387475+00', '2019-06-10 22:51:16.387475+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (107, 'ms', 'ms', 'may', 'Malay', '2019-06-10 22:51:16.39055+00', '2019-06-10 22:51:16.39055+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (108, 'mg', 'mg', 'mlg', 'Malagasy', '2019-06-10 22:51:16.393581+00', '2019-06-10 22:51:16.393581+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (109, 'mt', 'mt', 'mlt', 'Maltese', '2019-06-10 22:51:16.396682+00', '2019-06-10 22:51:16.396682+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (110, 'mn', 'mn', 'mon', 'Mongolian', '2019-06-10 22:51:16.399017+00', '2019-06-10 22:51:16.399017+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (111, 'na', 'na', 'nau', 'Nauru', '2019-06-10 22:51:16.401092+00', '2019-06-10 22:51:16.401092+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (112, 'nv', 'nv', 'nav', 'Navajo; Navaho', '2019-06-10 22:51:16.402768+00', '2019-06-10 22:51:16.402768+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (113, 'nr', 'nr', 'nbl', 'Ndebele, South; South Ndebele', '2019-06-10 22:51:16.404575+00', '2019-06-10 22:51:16.404575+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (114, 'nd', 'nd', 'nde', 'Ndebele, North; North Ndebele', '2019-06-10 22:51:16.406368+00', '2019-06-10 22:51:16.406368+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (115, 'ng', 'ng', 'ndo', 'Ndonga', '2019-06-10 22:51:16.408437+00', '2019-06-10 22:51:16.408437+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (116, 'ne', 'ne', 'nep', 'Nepali', '2019-06-10 22:51:16.410743+00', '2019-06-10 22:51:16.410743+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (117, 'nn', 'nn', 'nno', 'Norwegian Nynorsk; Nynorsk, Norwegian', '2019-06-10 22:51:16.413617+00', '2019-06-10 22:51:16.413617+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (118, 'nb', 'nb', 'nob', 'Bokmål, Norwegian; Norwegian Bokmål', '2019-06-10 22:51:16.416699+00', '2019-06-10 22:51:16.416699+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (119, 'no', 'no', 'nor', 'Norwegian', '2019-06-10 22:51:16.41973+00', '2019-06-10 22:51:16.41973+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (120, 'ny', 'ny', 'nya', 'Chichewa; Chewa; Nyanja', '2019-06-10 22:51:16.422789+00', '2019-06-10 22:51:16.422789+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (121, 'oc', 'oc', 'oci', 'Occitan (post 1500); Provençal', '2019-06-10 22:51:16.426307+00', '2019-06-10 22:51:16.426307+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (122, 'oj', 'oj', 'oji', 'Ojibwa', '2019-06-10 22:51:16.429447+00', '2019-06-10 22:51:16.429447+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (123, 'or', 'or', 'ori', 'Oriya', '2019-06-10 22:51:16.432577+00', '2019-06-10 22:51:16.432577+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (124, 'om', 'om', 'orm', 'Oromo', '2019-06-10 22:51:16.435938+00', '2019-06-10 22:51:16.435938+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (125, 'os', 'os', 'oss', 'Ossetian; Ossetic', '2019-06-10 22:51:16.43924+00', '2019-06-10 22:51:16.43924+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (126, 'pa', 'pa', 'pan', 'Panjabi; Punjabi', '2019-06-10 22:51:16.442462+00', '2019-06-10 22:51:16.442462+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (127, 'fa', 'fa', 'per', 'Persian', '2019-06-10 22:51:16.445582+00', '2019-06-10 22:51:16.445582+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (128, 'pi', 'pi', 'pli', 'Pali', '2019-06-10 22:51:16.448067+00', '2019-06-10 22:51:16.448067+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (129, 'pl', 'pl', 'pol', 'Polish', '2019-06-10 22:51:16.45033+00', '2019-06-10 22:51:16.45033+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (130, 'pt', 'pt', 'por', 'Portuguese', '2019-06-10 22:51:16.452407+00', '2019-06-10 22:51:16.452407+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (131, 'ps', 'ps', 'pus', 'Pushto; Pashto', '2019-06-10 22:51:16.454406+00', '2019-06-10 22:51:16.454406+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (132, 'qu', 'qu', 'que', 'Quechua', '2019-06-10 22:51:16.456366+00', '2019-06-10 22:51:16.456366+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (133, 'rm', 'rm', 'roh', 'Romansh', '2019-06-10 22:51:16.458706+00', '2019-06-10 22:51:16.458706+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (134, 'ro', 'ro', 'rum', 'Romanian; Moldavian; Moldovan', '2019-06-10 22:51:16.461011+00', '2019-06-10 22:51:16.461011+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (135, 'rn', 'rn', 'run', 'Rundi', '2019-06-10 22:51:16.463053+00', '2019-06-10 22:51:16.463053+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (136, 'ru', 'ru', 'rus', 'Russian', '2019-06-10 22:51:16.469679+00', '2019-06-10 22:51:16.469679+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (137, 'sg', 'sg', 'sag', 'Sango', '2019-06-10 22:51:16.473103+00', '2019-06-10 22:51:16.473103+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (138, 'sa', 'sa', 'san', 'Sanskrit', '2019-06-10 22:51:16.476465+00', '2019-06-10 22:51:16.476465+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (139, 'si', 'si', 'sin', 'Sinhala; Sinhalese', '2019-06-10 22:51:16.479704+00', '2019-06-10 22:51:16.479704+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (140, 'sk', 'sk', 'slo', 'Slovak', '2019-06-10 22:51:16.482913+00', '2019-06-10 22:51:16.482913+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (141, 'sl', 'sl', 'slv', 'Slovenian', '2019-06-10 22:51:16.486299+00', '2019-06-10 22:51:16.486299+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (142, 'se', 'se', 'sme', 'Northern Sami', '2019-06-10 22:51:16.489518+00', '2019-06-10 22:51:16.489518+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (143, 'sm', 'sm', 'smo', 'Samoan', '2019-06-10 22:51:16.492614+00', '2019-06-10 22:51:16.492614+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (144, 'sn', 'sn', 'sna', 'Shona', '2019-06-10 22:51:16.495817+00', '2019-06-10 22:51:16.495817+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (145, 'sd', 'sd', 'snd', 'Sindhi', '2019-06-10 22:51:16.499067+00', '2019-06-10 22:51:16.499067+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (146, 'so', 'so', 'som', 'Somali', '2019-06-10 22:51:16.502837+00', '2019-06-10 22:51:16.502837+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (147, 'st', 'st', 'sot', 'Sotho, Southern', '2019-06-10 22:51:16.506344+00', '2019-06-10 22:51:16.506344+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (148, 'es', 'es', 'spa', 'Spanish; Castilian', '2019-06-10 22:51:16.50965+00', '2019-06-10 22:51:16.50965+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (149, 'sc', 'sc', 'srd', 'Sardinian', '2019-06-10 22:51:16.512872+00', '2019-06-10 22:51:16.512872+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (150, 'sr', 'sr', 'srp', 'Serbian', '2019-06-10 22:51:16.516199+00', '2019-06-10 22:51:16.516199+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (151, 'ss', 'ss', 'ssw', 'Swati', '2019-06-10 22:51:16.520035+00', '2019-06-10 22:51:16.520035+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (152, 'su', 'su', 'sun', 'Sundanese', '2019-06-10 22:51:16.523915+00', '2019-06-10 22:51:16.523915+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (153, 'sw', 'sw', 'swa', 'Swahili', '2019-06-10 22:51:16.527482+00', '2019-06-10 22:51:16.527482+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (154, 'sv', 'sv', 'swe', 'Swedish', '2019-06-10 22:51:16.530776+00', '2019-06-10 22:51:16.530776+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (155, 'ty', 'ty', 'tah', 'Tahitian', '2019-06-10 22:51:16.533889+00', '2019-06-10 22:51:16.533889+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (156, 'ta', 'ta', 'tam', 'Tamil', '2019-06-10 22:51:16.537402+00', '2019-06-10 22:51:16.537402+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (157, 'tt', 'tt', 'tat', 'Tatar', '2019-06-10 22:51:16.540638+00', '2019-06-10 22:51:16.540638+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (158, 'te', 'te', 'tel', 'Telugu', '2019-06-10 22:51:16.543587+00', '2019-06-10 22:51:16.543587+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (159, 'tg', 'tg', 'tgk', 'Tajik', '2019-06-10 22:51:16.546578+00', '2019-06-10 22:51:16.546578+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (160, 'tl', 'tl', 'tgl', 'Tagalog', '2019-06-10 22:51:16.549665+00', '2019-06-10 22:51:16.549665+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (161, 'th', 'th', 'tha', 'Thai', '2019-06-10 22:51:16.554214+00', '2019-06-10 22:51:16.554214+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (162, 'bo', 'bo', 'tib', 'Tibetan', '2019-06-10 22:51:16.558239+00', '2019-06-10 22:51:16.558239+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (163, 'ti', 'ti', 'tir', 'Tigrinya', '2019-06-10 22:51:16.561649+00', '2019-06-10 22:51:16.561649+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (164, 'to', 'to', 'ton', 'Tonga (Tonga Islands)', '2019-06-10 22:51:16.564944+00', '2019-06-10 22:51:16.564944+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (165, 'tn', 'tn', 'tsn', 'Tswana', '2019-06-10 22:51:16.568152+00', '2019-06-10 22:51:16.568152+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (166, 'ts', 'ts', 'tso', 'Tsonga', '2019-06-10 22:51:16.572021+00', '2019-06-10 22:51:16.572021+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (167, 'tk', 'tk', 'tuk', 'Turkmen', '2019-06-10 22:51:16.57591+00', '2019-06-10 22:51:16.57591+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (168, 'tr', 'tr', 'tur', 'Turkish', '2019-06-10 22:51:16.579802+00', '2019-06-10 22:51:16.579802+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (169, 'tw', 'tw', 'twi', 'Twi', '2019-06-10 22:51:16.583533+00', '2019-06-10 22:51:16.583533+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (170, 'ug', 'ug', 'uig', 'Uighur; Uyghur', '2019-06-10 22:51:16.587607+00', '2019-06-10 22:51:16.587607+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (171, 'uk', 'uk', 'ukr', 'Ukrainian', '2019-06-10 22:51:16.595132+00', '2019-06-10 22:51:16.595132+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (172, 'ur', 'ur', 'urd', 'Urdu', '2019-06-10 22:51:16.598567+00', '2019-06-10 22:51:16.598567+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (173, 'uz', 'uz', 'uzb', 'Uzbek', '2019-06-10 22:51:16.601873+00', '2019-06-10 22:51:16.601873+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (174, 've', 've', 'ven', 'Venda', '2019-06-10 22:51:16.605143+00', '2019-06-10 22:51:16.605143+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (175, 'vi', 'vi', 'vie', 'Vietnamese', '2019-06-10 22:51:16.608414+00', '2019-06-10 22:51:16.608414+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (176, 'vo', 'vo', 'vol', 'Volapük', '2019-06-10 22:51:16.611652+00', '2019-06-10 22:51:16.611652+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (177, 'cy', 'cy', 'wel', 'Welsh', '2019-06-10 22:51:16.614955+00', '2019-06-10 22:51:16.614955+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (178, 'wa', 'wa', 'wln', 'Walloon', '2019-06-10 22:51:16.618384+00', '2019-06-10 22:51:16.618384+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (179, 'wo', 'wo', 'wol', 'Wolof', '2019-06-10 22:51:16.621621+00', '2019-06-10 22:51:16.621621+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (180, 'xh', 'xh', 'xho', 'Xhosa', '2019-06-10 22:51:16.625127+00', '2019-06-10 22:51:16.625127+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (181, 'yi', 'yi', 'yid', 'Yiddish', '2019-06-10 22:51:16.628409+00', '2019-06-10 22:51:16.628409+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (182, 'yo', 'yo', 'yor', 'Yoruba', '2019-06-10 22:51:16.632069+00', '2019-06-10 22:51:16.632069+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (183, 'za', 'za', 'zha', 'Zhuang; Chuang', '2019-06-10 22:51:16.635451+00', '2019-06-10 22:51:16.635451+00');
INSERT INTO teppelin_public.language (language_id, language_tag, alpha2, alpha3, language, created_at, updated_at) VALUES (184, 'zu', 'zu', 'zul', 'Zulu', '2019-06-10 22:51:16.639223+00', '2019-06-10 22:51:16.639223+00');


--
-- Name: language_language_id_seq; Type: SEQUENCE SET; Schema: teppelin_public; Owner: postgres
--

SELECT pg_catalog.setval('teppelin_public.language_language_id_seq', 184, true);


--
-- PostgreSQL database dump complete
--

-- Custom languages
INSERT INTO teppelin_public.language (language_tag, language) VALUES ('x-unk', 'Unknown');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('pt-BR', 'portuguese (brazilian)', 'pt', 'por');
INSERT INTO teppelin_public.language (language_tag, language) VALUES ('x-in', 'instrumental');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('zh-x-cmn', 'chinese (mandarin)', 'zh', 'chi');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('zh-x-yue', 'chinese (cantonese)', 'zh', 'chi');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('zh-x-nan', 'chinese (taiwanese)', 'zh', 'chi');
INSERT INTO teppelin_public.language (language_tag, language) VALUES ('x-other', 'other');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('x-jat', 'japanese (transcription)', 'ja', 'jpn');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('zh-Hant', 'chinese (traditional)', 'zh', 'chi');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('zh-Hans', 'chinese (simplified)', 'zh', 'chi');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('x-kot', 'korean (transcription)', 'ko', 'kor');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('es-LA', 'spanish (latin american)', 'es', 'spa');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('x-zht', 'chinese (transcription)', 'zh', 'chi');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('grc', 'greek (ancient)', 'el', 'gre');
INSERT INTO teppelin_public.language (language_tag, language, alpha2, alpha3) VALUES ('x-tht', 'thai (transcription)', 'th', 'tha');
