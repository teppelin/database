-- Base data seed (required for update script to run) --

-- ref: https://en.wikipedia.org/wiki/ISO/IEC_5218
insert into teppelin_public.gender(gender_id, gender) values
  (0, 'Unknown'),
  (1, 'Male'),
  (2, 'Female'),
  (9, 'Not Applicable');

insert into teppelin_public.cataloging_app(cataloging_app, base_url, resource_url)
values
  ('mangaupdates', 'https://www.mangaupdates.com/', 'https://www.mangaupdates.com/series.html?id='),
  ('novelupdates', 'https://www.novelupdates.com/', 'https://www.novelupdates.com/?p=');

-- States --
insert into teppelin_public.media_state (media_state) values ('completed');
insert into teppelin_public.media_state (media_state) values ('ongoing');
insert into teppelin_public.media_state (media_state) values ('unaired/unknown');

insert into teppelin_public.category (category) values
  ('novel'), ('anime'), ('manga');

insert into teppelin_public.chapter_type (chapter_type) values
  ('chapter'), ('prologue'), ('side story'), ('epilogue'), ('forward'), ('outro'), ('afterword'), ('other');

insert into teppelin_public.media_type (media_type) values
  ('web novel'),
  ('light novel'),
  ('korean novel'),
  ('chinese novel'),
  ('thai novel'),
  ('n/a'),
  ('vietnamese novel'),
  ('published novel');

-- insert into amqp.broker (host, port, username, password) values
--   ('usagi', '5672', 'admin', '123')
