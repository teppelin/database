# Database

DB probably started up separately.

## Running

Build and run.

`POSTGRES_DB`, `POSTGRES_PASSWORD` and `AUTH_USER_PASSWORD` env vars need to be passed to the container when running.

```sh
docker build -t teppelin_database .
docker run -e POSTGRES_PASSWORD=123 -e POSTGRES_DB=nekoani -e AUTH_USER_PASSWORD=secret -p 5432:5432 -d teppelin_database
```

Stack:

On the node you want to delpoy the DB on:

```sh
export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
docker node update --label-add teppelin.db=true $NODE_ID
docker stack deploy teppelin-pgsql
```

### Conventions

With the exception of `100_jobs.sql` which was imported from a previous project
and requires bringing in line, the SQL files in this repository try to adhere
to the conventions defined in [CONVENTIONS.md](./CONVENTIONS.md). PRs to fix
our adherence to these conventions would be welcome. Someone writing an SQL
equivalent of ESLint and/or prettier would be even more welcome!

### Directory structure

```
.
├── common      miscellaneous SQL files such as languages and types
├── core        core schema files
├── migrations  SQL files in migration format, ~~probably~~ will be used after the initial public release
└── restore     a simple Golang dump restore program
```

### TODO:

- [ ] use something like [SchemaSpy](http://schemaspy.org/) and make sure all entities are commented.
- [ ] actual roles instead of `is_admin` col on `person`?
