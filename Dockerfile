FROM postgres:13

RUN buildDependencies="ca-certificates \
  curl" \
  && apt-get update \
  && apt-get install -y --no-install-recommends ${buildDependencies} \
  && apt-get install -y postgresql-$PG_MAJOR-cron \
  && apt-get clean \
  && apt-get remove --purge -y ${buildDependencies} \
  && apt-get autoremove -y \
  && rm -rf /tmp/build /var/lib/apt/lists/*

COPY restore/restore /restore
COPY core/002_setup.sh /docker-entrypoint-initdb.d/002_setup.sh
COPY core/100_core.sql /docker-entrypoint-initdb.d/100_core.sql
COPY dump/current.sql /docker-entrypoint-initdb.d/999.sql
